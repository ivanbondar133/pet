# Setup —————————————————————————————————————————————————————————————————————————
PROJECT        = site
DOCKER_COMPOSE = docker-compose
DOCKER         = docker
.DEFAULT_GOAL  := help

.PHONY : help

# This will output the help for each task. thanks to https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## Show this help
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

## —— docker —————————————————————————————————————————————————————————————————
start: ## Run local environment
	$(DOCKER_COMPOSE) up -d --force-recreate --remove-orphans --no-color
build: ## Run local environment with rebuild containers
	$(DOCKER_COMPOSE) up -d --force-recreate --remove-orphans --build --no-color
down:
	$(DOCKER_COMPOSE) down --remove-orphans
