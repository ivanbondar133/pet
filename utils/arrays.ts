export const replaceItemAtIndex = (arr: Array<unknown>, index: number, newValue: unknown): Array<unknown> => {
  return [...arr.slice(0, index), newValue, ...arr.slice(index + 1)]
}
export const removeItemAtIndex = (arr: Array<unknown>, index: number): Array<unknown> => {
  return [...arr.slice(0, index), ...arr.slice(index + 1)]
}
