import { TextVariants } from '../components/helpers'

export const getTextVariant = (num: number): TextVariants => {
  return num > 0 ? TextVariants.SUCCESS : num < 0 ? TextVariants.ERROR : TextVariants.GRAY_DARK
}
