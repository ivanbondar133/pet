export type TAnimation = {
  hidden: {
    transition?: {
      duration?: number
      delay?: number
    }
    y?: number
    x?: number
    opacity?: number
    scale?: number
  }
  show: {
    transition?: {
      duration?: number
      delay?: number
      staggerChildren?: number
      delayChildren?: number
    }
    opacity?: number
    y?: number
    x?: number
    scale?: number
  }
}

export const scrollReveal: TAnimation = {
  hidden: {
    opacity: 0,
    transition: {
      duration: 1,
    },
  },
  show: {
    opacity: 1,
    transition: {
      duration: 1,
    },
  },
}

export const headerAnimation: TAnimation = {
  hidden: {
    opacity: 0,
    y: 100,
    transition: {
      duration: 1,
    },
  },
  show: {
    opacity: 1,
    y: 0,
    transition: {
      duration: 1,
    },
  },
}

export const fadeAnimation: TAnimation = {
  hidden: {
    opacity: 0,
    transition: {
      duration: 1,
    },
  },
  show: {
    opacity: 1,
    transition: {
      duration: 1,
    },
  },
}

export const fadeInLeftAnimation: TAnimation = {
  hidden: {
    opacity: 0,
    x: 100,
    transition: {
      duration: 1,
    },
  },
  show: {
    opacity: 1,
    x: 0,
    transition: {
      duration: 1,
    },
  },
}

export const containerAnimation = {
  hidden: {},
  show: {
    transition: {
      staggerChildren: 0.5,
    },
  },
}
