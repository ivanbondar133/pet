import React, {useState} from "react";

interface IStepNavContextProps {
	navHeight: number
	setNavHeight: React.Dispatch<React.SetStateAction<number>>
}

export const MenuContext = React.createContext<IStepNavContextProps>({
	navHeight: 60,
	setNavHeight: () => 60,
});

const MenuProvider = ({children}) => {
	const [navHeight, setNavHeight] = useState(60)

	return <MenuContext.Provider
		value={{navHeight, setNavHeight}}
	>{children}</MenuContext.Provider>
}

export default MenuProvider
