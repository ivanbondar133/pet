import { GetServerSideProps, GetServerSidePropsContext } from 'next'
import pino from 'pino'
import { logAndRedirectFromUnexpectedError } from './redirects'

export const createGetServerSideProps = (
  callback: (context: GetServerSidePropsContext) => ReturnType<GetServerSideProps>
) => {
  return async (context: GetServerSidePropsContext): Promise<ReturnType<GetServerSideProps>> => {
    try {
      return await callback(context)
    } catch (error) {
      return logAndRedirectFromUnexpectedError({
        error,
        msg: 'Unexpected error in createGetServerSideProps',
        route: context.resolvedUrl,
      })
    }
  }
}

export const logger = pino({
  level: 'debug',
  formatters: {
    level: (label, _number) => {
      return { level: label }
    },
  },
  timestamp: pino.stdTimeFunctions.isoTime,
})
