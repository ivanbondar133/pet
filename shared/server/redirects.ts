import { logger } from './index'

type NextJSRedirect = {
  redirect: {
    permanent: boolean
    destination: string
  }
}

interface LogAndRedirectFromUnexpectedErrorFn {
  msg: string
  error: unknown
  route?: string
}

export const logAndRedirectFromUnexpectedError = ({
  msg,
  error,
  route = '',
}: LogAndRedirectFromUnexpectedErrorFn): NextJSRedirect => {
  logger.error({ error, msg, route })
  return {
    redirect: {
      destination: `/something-went-wrong`,
      permanent: false,
    },
  }
}
