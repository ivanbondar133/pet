import React, { FC, useRef } from 'react'
import Menu from '../partial/Menu/Menu'
import styled from 'styled-components'
import { Box } from '../helpers'
import { useRouter } from 'next/router'
import { MenuContext } from 'contexts/MenuContext'

const SiteBox = styled.div<{ offset?: number }>`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  ${({ offset }) => offset && `padding-top: ${offset}px;`}
`

const Layout: FC = ({ children }): JSX.Element => {
  const { navHeight } = React.useContext(MenuContext)
  const router = useRouter()
  const ref = useRef(null)

  return (
    <>
      <div ref={ref} />
      <SiteBox offset={router.route === '/' ? navHeight : null}>
        <Menu />
        <Box flex={1} mt={navHeight}>
          {children}
        </Box>
      </SiteBox>
    </>
  )
}

export default Layout
