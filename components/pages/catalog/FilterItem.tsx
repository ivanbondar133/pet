import React, { FC, useState } from 'react'
import Checkbox from 'components/Forms/Checkbox'
import { Box, Flex } from 'components/helpers'
import Icon from 'components/helpers/Icon'
import { AnyObject } from 'types'
import { useUrlHelper } from 'hooks/useUrlHelper'
import { xor } from 'lodash'

type Props = {
  groupName: string
  filter: AnyObject
}

const FilterItem: FC<Props> = ({ groupName, filter, children }) => {
  const { query, updateQuery } = useUrlHelper()

  const [opened, setOpened] = useState(false)

  const handleCheck = (group, value) => {
    let currentState = query[group] ? query[group].split('|') : []
    if (Array.isArray(value)) {
      const valuesArray = value.map((item) => item.value)
      const notIncluded = valuesArray.filter((el) => !currentState.includes(el))
      if (notIncluded.length) {
        const newValues = currentState.concat(notIncluded)
        updateQuery({
          [group]: newValues.join('|'),
        })
      } else {
        const remainingValues = xor(currentState, valuesArray)
        updateQuery({
          [group]: remainingValues.join('|'),
        })
      }
    } else {
      if (currentState.includes(value)) {
        currentState = currentState.filter((item) => item !== value)
      } else {
        currentState.push(value)
      }
      if (!currentState.length) {
        updateQuery(group)
        return
      }
      updateQuery({
        [group]: currentState.join('|'),
      })
    }
  }

  const isChecked: boolean = (() => {
    if (Array.isArray(filter.value)) {
      const currentState = query[groupName]
      if (currentState) {
        const valuesArray = filter.value.map((item) => item.value)
        return valuesArray.every((el) => currentState.split('|').includes(el))
      }
      return false
    }
    return query[groupName] ? query[groupName].includes(filter.value) : false
  })()
  const isPartial: boolean = (() => {
    if (Array.isArray(filter.value) && query[groupName]) {
      const valuesArray = filter.value.map((item) => item.value)
      return valuesArray.some((el) => query[groupName].split('|').includes(el))
    }
    return false
  })()
  return (
    <Box>
      <Flex alignItems="center" mb={3}>
        <Checkbox checked={isChecked} onChange={() => handleCheck(groupName, filter.value)} partial={isPartial}>
          {filter.label}
        </Checkbox>
        {Array.isArray(filter.value) && (
          <Box className="pointer" ml="auto" onClick={() => setOpened(!opened)} p={2}>
            <Icon name={opened ? 'angle-up' : 'angle-down'} />
          </Box>
        )}
      </Flex>
      {opened && children}
    </Box>
  )
}

export default FilterItem
