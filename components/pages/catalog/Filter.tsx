import React, { FC } from 'react'
import { BorderedBox, Box, H4 } from 'components/helpers'
import FilterItem from './FilterItem'

const filters = [
  {
    label: 'Страна',
    name: 'countries',
    items: [
      {
        label: 'Россия',
        value: 'RU',
      },
      {
        label: 'США',
        value: 'US',
      },
    ],
  },
  {
    label: 'Валюта',
    name: 'currencies',
    items: [
      {
        label: 'Российский рубль',
        value: 'RUB',
      },
      {
        label: 'Доллар США',
        value: 'USD',
      },
      {
        label: 'Евро',
        value: 'EUR',
      },
    ],
  },
  {
    label: 'Отрасль',
    name: 'industry',
    items: [
      {
        label: 'Энергетика',
        value: [
          {
            label: 'Ископаемое горючее',
            value: 'ENERGY_FOSSIL_FUELS',
          },
          {
            label: 'Возобновляемая энергетика',
            value: 'RENEWABLE_ENERGY',
          },
        ],
      },
      {
        label: 'Потребительские товары длительного пользования',
        value: [
          {
            label: 'Автомобили и автозапчасти',
            value: 'AUTOMOBILES_AND_AUTO_PARTS',
          },
          {
            label: 'Потребительские товары',
            value: 'CYCLICAL_CONSUMER_PRODUCTS',
          },
          {
            label: 'Потребительские сервисы',
            value: 'CYCLICAL_CONSUMER_SERVICES',
          },
          {
            label: 'Ритейлеры',
            value: 'RETAILERS',
          },
        ],
      },
    ],
  },
]

const Filter: FC = () => {
  return (
    <BorderedBox p={5}>
      {filters.map((group) => {
        return (
          <div key={group.name}>
            <H4 mb={2} mt={6}>
              {group.label}
            </H4>
            {group.items.map((filter) => {
              return (
                <Box key={JSON.stringify(filter.value)} mb={3}>
                  <FilterItem filter={filter} groupName={group.name}>
                    {Array.isArray(filter.value) && (
                      <Box pl={4}>
                        {filter.value.map((innerFilter) => {
                          return <FilterItem filter={innerFilter} groupName={group.name} key={innerFilter.value} />
                        })}
                      </Box>
                    )}
                  </FilterItem>
                </Box>
              )
            })}
          </div>
        )
      })}
    </BorderedBox>
  )
}

export default Filter
