import { Box, Col, Grid, H3, Row, Text } from 'components/helpers'
import React, { FC } from 'react'
import Chart from '../../Chart'
import FinRow, { FinRowItemType } from '../../rows/FinRow'
import { GridItem } from '../../helpers/GridItem'
import SimilarShares from '../../Cards/SimilarShares'
import { similarShareStub } from '../../../stubs'

const finRowItem: FinRowItemType = {
  value: '25,21 млрд. ₽',
  description: 'Прибыль до вычета расходов',
  title: 'Ebitda',
}

console.log(finRowItem)

const OverView: FC = () => {
  return (
    <>
      <Chart />
      <Box my={8}>
        <H3 mb={6}>О компании</H3>
        <Text lineHeight={3} mb={8} />
      </Box>
      <H3 mb={6}>Оценка стоимости</H3>
      <Row justifyContent="space-between" mb={8}>
        <Col width={[1, 1, 1, 5 / 12]}>
          <FinRow item={finRowItem} />
          <FinRow item={finRowItem} />
        </Col>
        <Col width={[1, 1, 1, 5 / 12]}>
          <FinRow item={finRowItem} />
          <FinRow item={finRowItem} />
        </Col>
      </Row>
      <H3 mb={6}>Финансовые показатели</H3>
      <Row justifyContent="space-between" mb={8}>
        <Col width={[1, 1, 1, 5 / 12]}>
          <FinRow item={finRowItem} />
          <FinRow item={finRowItem} />
        </Col>
        <Col width={[1, 1, 1, 5 / 12]}>
          <FinRow item={finRowItem} />
          <FinRow item={finRowItem} />
        </Col>
      </Row>
      <H3 mb={6}>Акции из той же отросли</H3>
      <Grid mb={10} noPadding>
        <GridItem span={[4, 4, 4, 4]}>
          <SimilarShares item={similarShareStub} />
        </GridItem>
        <GridItem span={[4, 4, 4, 4]}>
          <SimilarShares item={similarShareStub} />
        </GridItem>
        <GridItem span={[4, 4, 4, 4]}>
          <SimilarShares item={similarShareStub} />
        </GridItem>
      </Grid>
    </>
  )
}

export default OverView
