import React, { useEffect, useState } from 'react'
import NewsCard from '../../Cards/NewsCard'
import axios from 'axios'

const ShareNews = () => {
  const [data, setData] = useState([])

  useEffect(() => {
    axios.get('https://swapi.dev/api/planets').then((r) => {
      setData(r.data)
    })
  }, [])

  return (
    <>
      <NewsCard mb={6} />
      <NewsCard mb={6} />
      <NewsCard mb={6} />
      <NewsCard mb={6} />
      <NewsCard mb={6} />
      <NewsCard mb={6} />
    </>
  )
}

export default ShareNews
