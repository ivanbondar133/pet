import React, {FC} from 'react';
import {Box, Container, Row, Col, Text} from '../../../helpers';
import {FontConfigurations} from '../../../../theme';
import Card from './Card';
import {containerAnimation, headerAnimation} from '../../../../utils/animation';
import Animated from '../../../helpers/Animated';

export type Tariff = {
	title: string,
	price: string,
	commission: number,
	accompaniment: boolean,
	plan: boolean,
	taxes: boolean,
	hit: boolean,
	volume?: string,
};

const tariffs: Tariff[] = [
	{
		title: 'Инвестор',
		price: '10.000',
		commission: 30,
		accompaniment: false,
		plan: false,
		taxes: false,
		hit: false,
	},
	{
		title: 'Трейдер',
		price: '20.000',
		commission: 25,
		accompaniment: false,
		plan: true,
		taxes: false,
		hit: false,
	},
	{
		title: 'Private Banking',
		price: '30.000',
		commission: 25,
		accompaniment: true,
		plan: true,
		taxes: true,
		hit: true,
		volume: '2 млн.',
	},
]

const Prices: FC = (): JSX.Element => {
	return (
		<Box id={'pricing'} pt={[80, 150, 130, 200]}>
			<Container>
				<Row>
					<Col width={[1, 1, 4 / 12, 1]}>
						<Animated variants={headerAnimation}>
							<Text as={'h2'} {...FontConfigurations.h2} bold mb={40}>
								Тарифы
							</Text>
						</Animated>
					</Col>
					<Col width={[1, 1, 8 / 12, 1]}>
						<Animated variants={containerAnimation}>
						<Row justifyContent={'center'}>
							{tariffs.map(tariff => {
								return (
									<Col variants={headerAnimation} mb={30} key={tariff.title} width={[1, 1 / 2, 1, 1 / 3]}>
										<Card tariff={tariff}/>
									</Col>
								)
							})}
						</Row>
						</Animated>
					</Col>
				</Row>
			</Container>
		</Box>
	)
}

export default Prices
