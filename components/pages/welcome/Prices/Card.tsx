import React from 'react'
import { Box, Flex, Text } from '../../../helpers'
import { Tariff } from './Prices'
import styled from 'styled-components'
import { colors, fontSizes, mediaQueries } from 'theme'

const border = '1px solid #e3e3e3'

type TProps = {
  tariff: Tariff
}

const LI = styled.li`
  display: flex;
  justify-content: space-between;
  position: relative;
  font-size: ${fontSizes[1]};
  padding-left: 15px;

  &:not(:last-child) {
    margin-bottom: 7px;
  }

  &:before {
    content: '';
    background: #ff1167;
    width: 5px;
    height: 5px;
    position: absolute;
    top: 8px;
    left: 0;
    border-radius: 50%;
  }
`

const Hit = styled.div`
  background: #fff1f6;
  box-shadow: 0 0 15px rgba(255, 17, 103, 0.25);
  width: 53px;
  height: 53px;
  -webkit-border-radius: 50%;
  -moz-border-radius: 50%;
  border-radius: 50%;
  position: absolute;
  top: 0;
  right: 0;
  transform: translate(0, -50%);
  z-index: 2;
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: Rubik, sans-serif;
  font-weight: 500;
  font-size: 20px;
  color: ${colors.primary};

  ${mediaQueries.medium} {
    transform: translate(50%, -50%);
  }
`

const Card = ({ tariff }: TProps): JSX.Element => {
  return (
    <Box border={border} borderRadius={10} height="100%" position="relative">
      {tariff.hit && <Hit>HIT</Hit>}
      <Flex
        alignItems="flex-start"
        borderBottom={border}
        justifyContent="space-between"
        minHeight={86}
        pb={2}
        pt={7}
        px={4}
      >
        <Text fontFamily="rubik" fontSize={4} lineHeight="30px">
          {tariff.title}
        </Text>
        <Text fontFamily="rubik">
          <div style={{ whiteSpace: 'nowrap' }}>
            <Text as="span" color="primary" fontSize={7}>
              {tariff.price}₽
            </Text>
            <Text as="span" color="lightGray" fontSize={3}>
              / мес.
            </Text>
          </div>
          {!!tariff.volume && (
            <Text color="lightGray" fontSize={[2, 3]} textAlign="right">
              Портфель от {tariff.volume} ₽
            </Text>
          )}
        </Text>
      </Flex>
      <Box flex={1} pb={4} pt={6} px={4}>
        <Text fontFamily="rubik" fontSize={3}>
          <ul>
            <LI>
              Комиссия с прибыли <Text>{tariff.commission}%</Text>
            </LI>
            <LI>
              Сопровождение по IPO и Pre-IPO <Text>{tariff.accompaniment ? 'Да' : '-'}</Text>
            </LI>
            <LI>
              Личный финансовый план <Text>{tariff.plan ? 'Да' : '-'}</Text>
            </LI>
            <LI>
              Налоговое планирование <Text>{tariff.taxes ? 'Да' : '-'}</Text>
            </LI>
          </ul>
        </Text>
      </Box>
    </Box>
  )
}

export default Card
