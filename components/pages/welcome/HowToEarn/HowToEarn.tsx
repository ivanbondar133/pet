import React from 'react'
import { Box, Container, Row, Col, Text, PreHeader } from '../../../helpers'
import { FontConfigurations } from 'theme'
import { fadeInLeftAnimation, headerAnimation } from 'utils/animation'
import Animated from '../../../helpers/Animated'

const HowToEarn = () => {
  return (
    <Box position="relative" pt={[80, 130, 190, 160]}>
      <Container>
        <Row justifyContent="space-between">
          <Col width={['100%', 1 / 2, 5 / 12, 1 / 3]}>
            <Animated variants={headerAnimation}>
              <PreHeader>Информация</PreHeader>
              <Text mb={[40, 0]} {...FontConfigurations.h2}>
                Как{' '}
                <Text as="span" color="primary">
                  зарабатывать на инвестициях
                </Text>
                , но&nbsp;не&nbsp;мониторить рынок каждый день
              </Text>
            </Animated>
          </Col>
          <Col width={['100%', 1 / 2, 7 / 12]}>
            <Animated variants={fadeInLeftAnimation}>
              <Box mb={[40, 80, 100, 240]}>
                <Text {...FontConfigurations.h3} mb={[4, 4, 2, 4]}>
                  Вы&nbsp;не&nbsp;тратите время на&nbsp;анализ рынка, <br /> а&nbsp;получаете готовые данные
                  от&nbsp;консультанта
                </Text>
                <Text fontSize={2} lineHeight={3}>
                  Анализ рынка&nbsp;&mdash; неизбежная часть инвестирования. Это рутинная работа, на&nbsp;которую уходят
                  целые дни. С&nbsp;инвестиционным консультантом вы&nbsp;сразу получаете готовую аналитику
                  от&nbsp;компании и&nbsp;знаете, в&nbsp;какие бумаги стоит инвестировать.
                </Text>
              </Box>
            </Animated>
            <Animated variants={fadeInLeftAnimation}>
              <Box mb={[40, 80, 100, 240]}>
                <Text {...FontConfigurations.h3} mb={[4, 4, 2, 4]}>
                  Вы&nbsp;не&nbsp;переживаете о&nbsp;падении акций или изменениях на&nbsp;рынке, а&nbsp;своевременно
                  получаете советы и&nbsp;поддержку от&nbsp;консультанта
                </Text>
                <Text fontSize={2} lineHeight={3}>
                  Наши консультанты будут держать вас в&nbsp;курсе всех событий, подсказывать с&nbsp;решениями
                  о&nbsp;покупке/продаже инструментов и&nbsp;тд. Консультант изучает тренды и&nbsp;связывается
                  с&nbsp;вами, если подворачивается возможность зафиксировать прибыль. Вы&nbsp;в&nbsp;это время
                  занимаетесь своими делами.
                </Text>
              </Box>
            </Animated>
            <Animated variants={fadeInLeftAnimation}>
              <Box>
                <Text {...FontConfigurations.h3} mb={[4, 4, 2, 4]}>
                  Вы&nbsp;платите только за&nbsp;качественную работу и&nbsp;помощь консультанта
                </Text>
                <Text fontSize={2} lineHeight={3}>
                  Большинство компаний берут процент за&nbsp;сделку. Если сделка окажется неудачной, то&nbsp;деньги
                  за&nbsp;помощь консультанта не&nbsp;вернут. Мы&nbsp;берем проценты только от&nbsp;прибыли. Если сделка
                  не&nbsp;приносит доходность, вы&nbsp;ничего не&nbsp;платите консультанту. Поэтому наши консультанты
                  в&nbsp;первую очередь заинтересованы в&nbsp;своевременной поддержке клиентов и&nbsp;тд
                </Text>
              </Box>
            </Animated>
          </Col>
        </Row>
      </Container>
    </Box>
  )
}

export default HowToEarn
