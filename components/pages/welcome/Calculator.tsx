import React, { useEffect, useState } from 'react'
import { Container, Row, Col, Text, Flex, Box } from '../../helpers'
import Animated from '../../helpers/Animated'
import { fadeInLeftAnimation, headerAnimation } from 'utils/animation'
import { colors, FontConfigurations, mediaQueries } from 'theme'
import sber from 'public/images/companies/sber.png'
import nlmk from 'public/images/companies/nlmk.png'
import afk from 'public/images/companies/AFK.png'
import match from 'public/images/companies/match.png'
import tesla from 'public/images/companies/tesla.png'
import apple from 'public/images/companies/apple.png'
import microsoft from 'public/images/companies/win.png'
import nike from 'public/images/companies/nike.png'
import gaz from 'public/images/companies/gaz.png'
import spotify from 'public/images/companies/spotify.png'
import Image from 'next/image'
import styled from 'styled-components'
import Index from '../../helpers/Button'
import Slider from 'react-custom-slider'
import { TImage } from 'types'
import angle from 'public/images/angle.svg'
import useWindowWidth from 'hooks/useWindowWidth'
import { breakpointsAsInts } from 'theme'

type TButtonProps = {
  active?: boolean
  title: string
}

const CompanyBtn = styled.button<TButtonProps>`
  width: 60px;
  height: 60px;
  border-radius: 50%;
  border: none;
  background: #FAFAFA;
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 0 0 40px;
  cursor: pointer;
  transition: all .3s ease;
  position: relative;
  ${({ active }) => !active && `filter: grayscale(100%);`}
  ${({ active, title }) =>
    !active &&
    ['Apple', 'Nike', 'Tesla'].includes(title) &&
    `
    img { opacity: 0.5;}
  `}
  ${mediaQueries.medium} {
    margin: 0 0 39px;
  }
  ${mediaQueries.large} {
    margin: 0 19px 36px;
  }
  }
  ${mediaQueries.xLarge} {
    margin: 0 9px 40px;
  }
  &:hover {
    background: #F0F0F0;
    span {
      opacity: 1;
    }
  }
  span {
    position: absolute;
    top: 63px;
    left: 50%;
    transform: translateX(-50%);
    transition: all .3s ease;
    font-weight: 500;
    font-size: 13px;
    line-height: 120%;
    opacity: ${({ active }) => Number(active)}
  }
  ${({ title }) =>
    title !== 'Nike' &&
    `
    img { max-width: 30px}
  `}
`

const YearBtn = styled(Index)`
  line-height: 40px;
  height: 40px;
  padding-left: 0;
  padding-right: 0;
  width: 160px;
  text-align: center;
  ${mediaQueries.medium} {
    width: 170px;
  }
  ${mediaQueries.large} {
    width: 200px;
  }
  ${mediaQueries.xLarge} {
    width: 170px;
  }
`

type Company = {
  img: TImage
  title: string
  multiplier: number[]
}

const companies: Company[] = [
  {
    img: sber,
    title: 'Sberbank',
    multiplier: [1.5405, 1.3005, 1.7888, 1.4176],
  },
  {
    img: nlmk,
    title: 'Novolipetsk Steel',
    multiplier: [1.5907, 1.5171, 1.4304, 1.4637],
  },
  {
    img: afk,
    title: 'AFK',
    multiplier: [1.5473, 2.2525, 3.7033, 1.7294],
  },
  {
    img: match,
    title: 'Match',
    multiplier: [1.3988],
  },
  {
    img: tesla,
    title: 'Tesla',
    multiplier: [2.2215, 16.126, 13.0552, 14.9769],
  },
  {
    img: apple,
    title: 'Apple',
    multiplier: [1.3677, 2.8938, 2.7662, 4.4505],
  },
  {
    img: microsoft,
    title: 'Microsoft',
    multiplier: [1.4859, 2.1649, 2.7694, 4.1027],
  },
  {
    img: nike,
    title: 'Nike',
    multiplier: [1.4486, 1.8411, 2.031, 2.095],
  },
  {
    img: gaz,
    title: 'Gazprom',
    multiplier: [1.858, 1.2248, 2.0526, 2.137],
  },
  {
    img: spotify,
    title: 'Spotify',
    multiplier: [1.0376, 1.8304, 1.4185],
  },
]

const Calculator = (): JSX.Element => {
  const [currentCompany, setCurrentCompany] = useState<Company>(companies[0])
  const [year, setYear] = useState(1)
  const [slider, setSlider] = useState(5000)
  const [total, setTotal] = useState<null | number>(null)
  const [trackLength, setTrackLength] = useState<number>(100)

  const { width } = useWindowWidth()

  useEffect(() => {
    if (width >= breakpointsAsInts[2]) {
      setTrackLength(750)
    } else if (width >= breakpointsAsInts[1]) {
      setTrackLength(940)
    } else if (width >= breakpointsAsInts[0]) {
      setTrackLength(718)
    } else {
      setTrackLength(width - 30)
    }
  }, [width])

  useEffect(() => {
    setYear(0)
  }, [currentCompany])

  useEffect(() => {
    const _total = slider * currentCompany.multiplier[year]
    setTotal(_total)
  }, [currentCompany, year, slider])

  return (
    <Container>
      <Row>
        <Col width={[1, 6 / 12, 1, 1 / 3]}>
          <Animated variants={headerAnimation}>
            <Text as="h2" {...FontConfigurations.h2} mb={[4, 7, 7, 0]}>
              Что, если бы вы вложили в...
            </Text>
          </Animated>
        </Col>
        <Col width={[1, 6 / 12, 1, 2 / 3]}>
          <Animated variants={fadeInLeftAnimation}>
            <Flex flexWrap="wrap" justifyContent={['center', 'flex-start']} mx={[0, 0, '-19px', '-9px']}>
              {companies.map((c) => (
                <Box key={c.title} width={[1 / 4, 1 / 4, 'auto', 'auto']}>
                  <CompanyBtn
                    active={currentCompany.title === c.title}
                    onClick={() => setCurrentCompany(c)}
                    title={c.title}
                  >
                    <img alt={c.title} src={c.img.src} />
                    <span>{c.title}</span>
                  </CompanyBtn>
                </Box>
              ))}
            </Flex>
          </Animated>
        </Col>
      </Row>
      <Row>
        <Col width={[1, null, null, 1 / 3]} />
        <Col width={[1, null, null, 2 / 3]}>
          <Animated variants={fadeInLeftAnimation}>
            <Flex flexWrap={['wrap', 'nowrap']} justifyContent="space-between" mb={[17, 33]}>
              {currentCompany.multiplier.map((m, idx) => {
                return (
                  <YearBtn
                    key={idx}
                    mb={[17, 0]}
                    onClick={() => setYear(idx)}
                    variant={year === idx ? 'primary' : 'outlined'}
                  >
                    {idx + 1} {idx === 0 ? 'год' : 'года'}
                  </YearBtn>
                )
              })}
            </Flex>
            <Flex alignItems="center" flexDirection={['column', 'row']} justifyContent="space-between" mb={4}>
              <Text fontWeight={500} mb={[2, 0]}>
                Если бы вложили
              </Text>
              <Text fontSize={20} fontWeight={700}>
                {slider}$
              </Text>
            </Flex>
            <Flex left={-10} position="relative">
              <Slider
                fillColor={colors.primary}
                handlerBorderColor={colors.primary}
                handlerColor={colors.primary}
                handlerShape="rounded"
                markers={0}
                markersSize={0}
                max={10000}
                min={100}
                onChange={(value) => setSlider(value)}
                showValue={false}
                trackColor="#DEDEDE"
                trackLength={trackLength}
                trackStyle={{
                  width: trackLength + 'px',
                }}
                value={slider}
              />
            </Flex>
            <Flex flexDirection={['row', 'column']} justifyContent="space-between">
              <Text mb={3} mt={23}>
                У вас было бы
              </Text>
              <Box>
                {total && (
                  <Text fontSize={30} fontWeight={700}>
                    {total.toFixed(0)}$
                  </Text>
                )}
                <Flex alignItems="center" mt="3px">
                  <Image alt="angle" src={angle} />
                  <Text color="success" fontSize={20}>
                    {' '}
                    {((currentCompany.multiplier[year] - 1) * 100).toFixed(2)}%
                  </Text>
                </Flex>
              </Box>
            </Flex>
          </Animated>
        </Col>
      </Row>
    </Container>
  )
}

export default Calculator
