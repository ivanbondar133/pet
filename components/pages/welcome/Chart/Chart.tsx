import React from 'react'
import { Box, Container, Row, Col, PreHeader, Text, Flex } from '../../../helpers'
import Image from 'next/image'
import chart from 'public/images/chart_2x.png'
import chartSm from 'public/images/chart-sm_2x.png'
import ChartInfo from 'public/images/chart-info_2x.png'
import { fadeInLeftAnimation, headerAnimation } from 'utils/animation'
import Animated from '../../../helpers/Animated'
import Button from '../../../helpers/Button'
import { breakpointsAsInts, FontConfigurations } from 'theme'
import useWindowWidth from 'hooks/useWindowWidth'

export default function Chart(): JSX.Element {
  const { width } = useWindowWidth()

  const isMobile = width <= breakpointsAsInts[0] + 1

  const img = isMobile ? chartSm : chart

  return (
    <Box>
      <Container pb={[100, 125, 150, 190]} pt={[100, 130, 95, 160]}>
        <Row>
          <Col width={[1, 1 / 2, 1 / 2, 1 / 3]}>
            <Animated variants={headerAnimation}>
              <PreHeader>Риски</PreHeader>

              <Text as="h2" {...FontConfigurations.h2} mb={[20]}>
                Распределите <br /> портфель,{' '}
                <Text as="span" variant="primary">
                  снижая <br /> риски
                </Text>
              </Text>

              <Text mb={35}>
                Диверсификация в&nbsp;инвестировании&nbsp;&mdash; надёжный способ сохранить финансы. При выборе ценных
                бумаг стоит помнить об&nbsp;их&nbsp;рисках и&nbsp;выделять соответствующий объём портфеля для каждого
                вида инвестиций
              </Text>

              <Box display={['none', 'block']}>
                <a href="/">
                  <Button minWidth={200} variant="primary">
                    Начать
                  </Button>
                </a>
              </Box>
            </Animated>
          </Col>
          <Col width={[1, 1 / 2, 1 / 2, 2 / 3]}>
            <Animated variants={fadeInLeftAnimation}>
              <Flex
                alignItems="center"
                flexDirection={['row', 'column']}
                justifyContent={['center', 'center', 'flex-end']}
              >
                <Image alt="chart" height={img.height / 2} src={img.src} width={img.width / 2} />
                <Box display={[null, null, 'none']} ml={[2, 0]} mt={2}>
                  <Image alt="chart" height={ChartInfo.height / 2} src={ChartInfo.src} width={ChartInfo.width / 2} />
                </Box>
              </Flex>
            </Animated>
          </Col>
        </Row>
        <Animated variants={headerAnimation}>
          <Box display={['block', 'none']} mt={45}>
            <a href="/">
              <Button variant="primary" width="100%">
                Начать
              </Button>
            </a>
          </Box>
        </Animated>
      </Container>
    </Box>
  )
}
