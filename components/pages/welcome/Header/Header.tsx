import React, { FC } from 'react'
import { Container, Row, Col, Text, Box, H1 } from '../../../helpers'
import Button from '../../../helpers/Button'
import Image from 'next/image'
import HeaderImg from 'public/images/header_2x.png'
import { fadeAnimation, headerAnimation } from 'utils/animation'
import Animated from '../../../helpers/Animated'
import styled from 'styled-components'
import theme, { mediaQueries } from 'theme'

const ImgWrap = styled(Box)`
  right: -300px;
  ${mediaQueries.medium} {
    right: -200px;
  }
  ${mediaQueries.large} {
    right: -300px;
  }
  @media (min-width: 1600px) {
    right: 0;
  }
`

const Header: FC = (): JSX.Element => {
  return (
    <Box overflow="hidden" position="relative">
      <Container>
        <Row>
          <Col
            pb={[0, 0, 120, 200]}
            position="relative"
            pt={[60, 92, 90, 200]}
            width={['100%', null, null, 5 / 12]}
            zIndex={theme.layers.indexOf('top')}
          >
            <Animated variants={headerAnimation}>
              <H1>
                Инвестиционное
                <br />
                консультирование
              </H1>
              <Text fontSize={5} fontWeight={500} my={3}>
                Мы знаем, как&nbsp;сохранить и&nbsp;заработать
              </Text>
              <a href="/">
                <Button minWidth={200} variant="primary">
                  Начать
                </Button>
              </a>
            </Animated>
          </Col>
        </Row>
      </Container>
      <ImgWrap
        bottom={[-300, -120, -70, -50]}
        display={['none', 'block']}
        position="absolute"
        width={['auto', 700, 900, 'auto']}
        zIndex={-1}
      >
        <Animated variants={fadeAnimation}>
          <Image alt="" height={HeaderImg.height / 2} src={HeaderImg.src} width={HeaderImg.width / 2} />
        </Animated>
      </ImgWrap>
    </Box>
  )
}

export default Header
