import React from 'react'
import { Box, Container, Row, Col, Text } from '../../../helpers'
import { FontConfigurations } from '../../../../theme'
import styled from 'styled-components'
import { containerAnimation, fadeInLeftAnimation, headerAnimation } from '../../../../utils/animation'
import Animated from '../../../helpers/Animated'

const Hr = styled.div`
  width: 70px;
  height: 1px;
  background: #b7b1b1;
  margin: 14px 0;
`
const Item = ({ children }) => (
  <Text color="primary" fontFamily="rubik" fontSize={[60, 40, 50, 60]}>
    {children}
    <Hr />
  </Text>
)

const textProps = {
  color: '#828282',
  fontSize: 15,
}

const About = () => {
  return (
    <Box id="about" pt={[50, 90, 90, 170]}>
      <Container>
        <Row mb={[30, 60, 80]}>
          <Col width={[1, 1 / 3]}>
            <Animated variants={headerAnimation}>
              <Text as="h2" mb={[4, 0]} {...FontConfigurations.h2}>
                О компании
              </Text>
            </Animated>
          </Col>
          <Col width={[1, 2 / 3]}>
            <Animated variants={fadeInLeftAnimation}>
              <Text fontSize={2} lineHeight={3}>
                Ведущие менеджеры из&nbsp;крупных финансовых компаний объединились и&nbsp;отстаивают интересы клиентов.
                Мы&nbsp;ищем все инвестиционные решения на&nbsp;рынке и&nbsp;предлагаем одни из&nbsp;самых интересных
                продуктов, которые позволяют сохранить и&nbsp;приумножить ваши средства.
              </Text>
            </Animated>
          </Col>
        </Row>
        <Row>
          <Col width={[0, 0, 1, 1]} />
          <Col width={1}>
            <Animated variants={containerAnimation}>
              <Row>
                <Col display={['none', 'none', 'block']} width={[0, 0, 4 / 12]} />
                <Col variants={fadeInLeftAnimation} width={[1, 1 / 3, 3 / 12]}>
                  <Item>1.440</Item>
                  <Text {...textProps}>Акций</Text>
                </Col>
                <Col variants={fadeInLeftAnimation} width={[1, 1 / 3, 3 / 12]}>
                  <Item>130</Item>
                  <Text {...textProps}>Облигаций</Text>
                </Col>
                <Col variants={fadeInLeftAnimation} width={[1, 1 / 3, 2 / 12]}>
                  <Item>32</Item>
                  <Text {...textProps}>Фонда</Text>
                </Col>
              </Row>
            </Animated>
          </Col>
        </Row>
      </Container>
    </Box>
  )
}

export default About
