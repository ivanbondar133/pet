import React from 'react'
import { Container, Row, Col, PreHeader, Box } from '../../../helpers'
import { Text } from '../../../helpers'
import Card from './Card'
import Chart from 'public/images/IChart.svg'
import Chain from 'public/images/IChain.svg'
import IHands from 'public/images/IHands.svg'
import ICoin from 'public/images/ICoin.svg'
import { FontConfigurations } from '../../../../theme'
import { containerAnimation, headerAnimation } from '../../../../utils/animation'
import Animated from '../../../helpers/Animated'

type Props = {}

export default function Hiw({}: Props) {
  return (
    <Box>
      <Container pb={[50, 90, 90, 140]} pt={[80, 130, 140, 190]}>
        <Row>
          <Col width={['auto', 1 / 2, 'auto', 1 / 3]}>
            <Animated variants={headerAnimation}>
              <PreHeader>Как это работает</PreHeader>
              <Text as="h2" mb={[20, 20, 30, 30]} {...FontConfigurations.h2}>
                Сформируйте инвестиционные продукты, которые подходят именно вам
              </Text>
            </Animated>
          </Col>
          <Col width={['auto', 1 / 2, 'auto', 2 / 3]}>
            <Animated variants={containerAnimation}>
              <Row>
                <Card
                  img={Chart}
                  text={
                    'Чтобы выбрать перспективную компанию, вовремя вложить деньги и&nbsp;наращивать капитал, теории недостаточно. Надо начинать инвестировать и&nbsp;глубже погружаться в&nbsp;нюансы рынка'
                  }
                  title="Одной теории недостаточно"
                />
                <Card
                  img={Chain}
                  text={
                    'Мы&nbsp;изучаем все интересные предложения и&nbsp;делимся этим анализом с&nbsp;клиентом. Вам остается только выбрать компании из&nbsp;списка предложенных'
                  }
                  title="Освободим от рутины"
                />
                <Card
                  img={IHands}
                  text={
                    'Вы&nbsp;можете открыть или уже иметь счет в&nbsp;другой компании. Мы&nbsp;просто будем вашими советниками и&nbsp;поможем с&nbsp;аналитикой'
                  }
                  title="Поможем с аналитикой"
                />
                <Card
                  img={ICoin}
                  text={
                    'Вы&nbsp;сэкономите время на&nbsp;обучение и&nbsp;вместе с&nbsp;брокером сможете выбрать понравившиеся компании, бизнес которых вам знаком и&nbsp;понятен'
                  }
                  title="Выберите готовые предложения"
                />
              </Row>
            </Animated>
          </Col>
        </Row>
      </Container>
    </Box>
  )
}
