import React from 'react'
import { Box, Col, Flex, Text } from '../../../helpers'
import Image from 'next/image'
import { TImage } from 'types'
import { fadeInLeftAnimation } from 'utils/animation'

type Props = {
  img: TImage
  title: string
  text: string
}

export default function Card({ img, title, text }: Props): JSX.Element {
  return (
    <Col mb="20px" variants={fadeInLeftAnimation} width={['auto', 'auto', 1 / 2]}>
      <Box border="1px solid" borderColor="#F2F2F2" borderRadius="10px" height="100%" px={4} py={7}>
        <Flex alignItems="center" mb="10px">
          <Image alt="icon" height={img.height} src={img.src} width={img.width} />
          <Text fontSize="15px" fontWeight={700} ml="10px">
            {title}
          </Text>
        </Flex>
        {/* eslint-disable-next-line @typescript-eslint/naming-convention */}
        <Text dangerouslySetInnerHTML={{ __html: text }} fontSize={14} fontWeight={500} lineHeight={3} />
      </Box>
    </Col>
  )
}
