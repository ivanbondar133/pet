import React from 'react'
import { Box, Col, Container, PreHeader, Row, Text, Flex } from '../../helpers'
import { FontConfigurations } from 'theme'
import Button from '../../helpers/Button'
import Image from 'next/image'
import one from 'public/images/one.svg'
import two from 'public/images/two.svg'
import three from 'public/images/three.svg'
import four from 'public/images/four.svg'
import styled from 'styled-components'
import { containerAnimation, fadeInLeftAnimation, headerAnimation } from 'utils/animation'
import Animated from '../../helpers/Animated'

const Spacer = styled(Box)`
  height: 100px;
  width: 10px;
  border-left: 1.5px dashed #ff1167;
  position: relative;
  left: 40.5px;
`

const steps = [
  {
    img: one,
    text: 'Оставьте заявку на нашем сайте',
  },
  {
    img: two,
    text: 'Познакомьтесь с&nbsp;вашим инвестиционным консультантом',
  },
  {
    img: three,
    text: 'Совместно разработайте индивидуальную стратегию',
  },
  {
    img: four,
    text: 'Начните торговать и&nbsp;зарабатывать!',
  },
]

const Steps = () => {
  return (
    <Box id="consultation" pb={[80, 120, 120, 180]} pt={[80, 130, 120, 160]}>
      <Container>
        <Row>
          <Col width={[1, 5 / 12]}>
            <Animated variants={headerAnimation}>
              <PreHeader>Этапы</PreHeader>
              <Text as="h2" mb={25} {...FontConfigurations.h2}>
                Всего{' '}
                <Text as="span" variant="primary">
                  4 шага
                </Text>{' '}
                <br />к инвестициям
              </Text>
              <Box display={['none', 'block']}>
                <a href="/">
                  <Button minWidth={200} variant="primary">
                    Начать
                  </Button>
                </a>
              </Box>
            </Animated>
          </Col>
          <Col width={[1, 7 / 12]}>
            {steps.map((step, i) => {
              return (
                <Animated key={i} variants={containerAnimation}>
                  <Flex alignItems="center">
                    <Box flexShrink={0} variants={headerAnimation} width={80}>
                      <Image src={step.img} />
                    </Box>
                    <Box ml={20} variants={fadeInLeftAnimation}>
                      <Text color="#000" dangerouslySetInnerHTML={{ __html: step.text }} />
                    </Box>
                  </Flex>
                  {i < steps.length - 1 && <Spacer variants={headerAnimation} />}
                </Animated>
              )
            })}
          </Col>
        </Row>
        <Box display={['block', 'none']} mt={40}>
          <a href="/">
            <Button minWidth={200} variant="primary">
              Начать
            </Button>
          </a>
        </Box>
      </Container>
    </Box>
  )
}

export default Steps
