import React from 'react'
import { Box, Flex, Container, Text, Grid } from '../../../helpers'
import Image from 'next/image'
import rus from 'public/images/rus.svg'
import cfra from 'public/images/cfra.svg'
import cpo from 'public/images/cpo.svg'
import { containerAnimation, fadeAnimation } from 'utils/animation'
import Animated from '../../../helpers/Animated'
import { GridItem } from '../../../helpers/GridItem'

type Props = {}

const FeatureItem = ({ img, children, ...props }) => {
  return (
    <Flex alignItems="center" {...props}>
      <Box flexShrink={0} mr={2} width={80}>
        <Image {...img} alt="icon" />
      </Box>
      <Text fontSize={[1, 1, 1, 2]}>{children}</Text>
    </Flex>
  )
}

export default function Features({}: Props): JSX.Element {
  return (
    <Box bg="#fff" border="1px solid" borderColor="#F5F5F5" boxShadow="0px 0px 10px rgba(208, 208, 208, 0.25)" py={8}>
      <Container>
        <Animated variants={containerAnimation}>
          <Grid>
            <GridItem span={[4, 4, 4, 4]}>
              <FeatureItem img={rus} variants={fadeAnimation}>
                Реестр Банка <br /> России на&nbsp;статус инвестиционного советника
              </FeatureItem>
            </GridItem>
            <GridItem span={[4, 4, 4, 4]}>
              <FeatureItem img={cfra} variants={fadeAnimation}>
                Аналитический партнёр группы&nbsp;&mdash; CFRA (Standard &amp;&nbsp;Poors)
              </FeatureItem>
            </GridItem>
            <GridItem span={[4, 4, 4, 4]}>
              <FeatureItem img={cpo} variants={fadeAnimation}>
                Членство в&nbsp;CPO &laquo;Ассоциация международных инвестионных консультантов и&nbsp;советников&raquo;
              </FeatureItem>
            </GridItem>
          </Grid>
        </Animated>
      </Container>
    </Box>
  )
}
