import React from 'react'
import { Box, Col, Text } from '../../../helpers'
import { NewsItem } from './News'
import styled from 'styled-components'
import { fadeAnimation } from 'utils/animation'
import Link from 'next/link'
import theme from 'theme'

const Wrap = styled(Box)<{ isNewsPage?: boolean }>`
  background: rgba(255, 255, 255, 0.1);
  border: 1px solid ${({ isNewsPage }) => (isNewsPage ? 'rgba(217, 217, 217, 0.5)' : 'rgba(255, 255, 255, 0.5)')};
  box-sizing: border-box;
  backdrop-filter: blur(30px);
  border-radius: 10px;
  padding: 30px 20px;
  height: 100%;
  display: flex;
  flex-direction: column;
`

const Lnk = styled.span`
  cursor: pointer;
  color: ${theme.colors.primary};
  text-decoration: underline;
`

type TProps = {
  item: NewsItem
  width: number | Array<number>
  isNewsPage?: boolean
}

const NewsCard = ({ item, width, isNewsPage }: TProps): JSX.Element => {
  return (
    <Col my={3} variants={isNewsPage ? null : fadeAnimation} width={width}>
      <Wrap isNewsPage={isNewsPage}>
        <Text fontSize={5} fontWeight={600}>
          {new Date(item.pd).toLocaleDateString()}
        </Text>
        <Text fontSize={1} my={2}>
          {item.h}
        </Text>
        <Box mt="auto">
          <Link href={`/news/${item.i}`}>
            <Lnk>Подробнее</Lnk>
          </Link>
        </Box>
      </Wrap>
    </Col>
  )
}

NewsCard.defaultProps = {
  width: [1, 1 / 2],
}

export default NewsCard
