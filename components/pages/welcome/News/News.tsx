import React, { FC, useEffect, useState } from 'react'
import { Box, Container, Col, Row, Text, PreHeader } from 'components/helpers'
import theme, { FontConfigurations, mediaQueries } from 'theme'
import NewsCard from './NewsCard'
import ellipse from 'public/images/elipse.svg'
import Image from 'next/image'
import styled from 'styled-components'
import { containerAnimation, fadeAnimation, headerAnimation } from 'utils/animation'
import Animated from 'components/helpers/Animated'
import useAxios from 'hooks/useAxios'
import Link from 'next/link'
import Button from 'components/helpers/Button'

export type NewsItem = {
  h: string
  pd: string
  i: number
}

const CircleWrap = styled(Box)`
  position: absolute;
  display: none;
  z-index: -1;

  ${mediaQueries.medium} {
    display: block;
    transform: translate(-50%, -50%);
    left: 50%;
    top: 50%;
    right: auto;
    max-width: 100%;
    max-height: 100%;
  }
`

const CircleRight = styled(Box)`
  right: 0;
  top: 50%;
  position: absolute;
  z-index: -1;
  transform: translate(50%, -50%);

  ${mediaQueries.medium} {
    display: none;
  }
`

const CircleLeft = styled(Box)`
  left: 0;
  top: 50%;
  position: absolute;
  z-index: -1;
  transform: translate(-50%, -50%);

  ${mediaQueries.medium} {
    display: none;
  }
`
const News: FC = () => {
  const { axios } = useAxios()
  const [news, setNews] = useState([])

  useEffect(() => {
    axios.get('/api/v3/finmarket/news/index').then(({ data }) => {
      const resp = data?.mbnl?.c_nwli
      if (resp && resp.length) {
        setNews(resp.slice(0, 4) || [])
      }
    })
  }, [])

  if (!news.length) {
    return null
  }

  return (
    <Box id="news" overflowX="hidden" pb={80} pt={[100, 80, 120, 160]}>
      <Container>
        <Row>
          <Col width={[1, 1, 1, 1 / 4]}>
            <Animated variants={headerAnimation}>
              <PreHeader>СМИ</PreHeader>
              <Text as="h2" mb={2} {...FontConfigurations.h2}>
                Новости
              </Text>
              <Link href="/news">
                <Button variant="primary">Все новости</Button>
              </Link>
            </Animated>
          </Col>
          <Col display={['none', 'block']} width={[1, 1, 1, 3 / 4]}>
            <Animated variants={containerAnimation}>
              <Row position="relative" zIndex={theme.layers.indexOf('top')}>
                {news.map((item) => (
                  <NewsCard item={item} key={item.i} />
                ))}
                <CircleWrap variants={fadeAnimation}>
                  <Image alt="" src={ellipse} />
                </CircleWrap>
              </Row>
            </Animated>
          </Col>
          {news.length && (
            <Col display={['block', 'none']} width={1}>
              <Animated variants={containerAnimation}>
                <Row position="relative" zIndex={theme.layers.indexOf('top')}>
                  <CircleRight>
                    <Image alt="" src={ellipse} />
                  </CircleRight>
                  <NewsCard item={news[0]} />
                  <NewsCard item={news[1]} />
                </Row>
                <Row position="relative" zIndex={theme.layers.indexOf('top')}>
                  <CircleLeft>
                    <Image alt="" src={ellipse} />
                  </CircleLeft>
                  <NewsCard item={news[2]} />
                  <NewsCard item={news[3]} />
                </Row>
              </Animated>
            </Col>
          )}
        </Row>
      </Container>
    </Box>
  )
}

export default News
