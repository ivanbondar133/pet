import React, {useEffect, useState} from 'react';
import {Box} from "./helpers";
import { useInView } from 'react-intersection-observer';
import { useScrollPosition } from '@n8tb1t/use-scroll-position'
import styled from "styled-components";

const Par = styled(Box)`
  -webkit-transform: translateZ(-300px);
  transform: translateZ(-300px);
  z-index: -1;
`

const Parallax = ({children, ...props}) => {

    // const { ref, inView, entry } = useInView({
    //     threshold: 0,
    // });
    //
    // const [dir, setDir] = useState('bottom')
    //
    // const [pos, setPos] = useState(0);
    //
    // useScrollPosition(({ prevPos, currPos }) => {
    //     const dir = currPos.y < prevPos.y ? 'bottom' : 'up'
    //     if (inView) {
    //         if (dir === 'bottom') {
    //             setPos((prev) => prev + .5)
    //         } else {
    //             setPos((prev) => prev - .5)
    //         }
    //
    //     }
    // })

    return <Par {...props}>
        {children}
    </Par>
}

export default Parallax
