import React, { FC } from 'react'
import { Box, Container } from '../helpers'

type TProps = {
  image: string
}

const InnerPageHeader: FC<TProps> = ({ children, image }) => {
  return (
    <Box
      backgroundImage={`url('${image}')`}
      backgroundPosition="center"
      backgroundSize="cover"
      height={[300, 300, 300, 555]}
    >
      <Container pt={[37, 37, 37, 70]}>{children}</Container>
    </Box>
  )
}

export default InnerPageHeader
