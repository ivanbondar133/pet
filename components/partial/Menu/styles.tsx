import styled from 'styled-components'
import theme, { mediaQueries } from 'theme'
import { Box } from '../../helpers'

export const LKLink = styled.a`
  align-items: center;
  color: ${({ theme }) => theme.colors.grayDark};
  text-decoration: none !important;
  transition: all 0.3s ease;
  margin-left: auto;
  height: 30px;
  ${mediaQueries.medium} {
    display: flex;
  }

  :hover {
    color: ${({ theme }) => theme.colors.primary};
  }

  span {
    display: none;
    ${mediaQueries.xLarge} {
      display: inline;
    }
  }
`

export const MenuWrap = styled(Box)`
  position: fixed;
  z-index: ${theme.layers.indexOf('menu')};
  top: 0;
  width: 100vw;
  background: #fff;
`

export const MenuLink = styled.a`
  cursor: pointer;
  text-decoration: none !important;
  color: ${({ theme }) => theme.colors.grayDark};
  transition: all 0.3s ease;
  display: inline-block;
  padding: 15px 12px;
  font-size: 13px;

  ${mediaQueries.medium} {
    padding: 5px 12px;
  }

  ${mediaQueries.large} {
    font-size: 15px;
  }

  :hover {
    color: ${({ theme }) => theme.colors.primary};
  }
`

export const HamBtn = styled.button<{ isDrawerOpen: boolean }>`
  background: ${theme.colors.primary};
  border: 0;
  width: 30px;
  height: 30px;
  border-radius: 5px;
  display: inline-flex;
  flex-direction: column;
  justify-content: space-between;
  padding: 10px 7px 9px;
  margin-left: 20px;
  &:before,
  &:after,
  span {
    width: 16px;
    height: 1px;
    background: ${theme.colors.pink};
    content: '';
    transition: all 0.3s ease;
    position: relative;
  }
  &:before {
    ${({ isDrawerOpen }) => isDrawerOpen && 'transform: rotate(45deg); top: 5px;'}
  }
  &:after {
    ${({ isDrawerOpen }) => isDrawerOpen && 'transform: rotate(-45deg); top: -5px;'}
  }
  span {
    width: 12px;
    margin-left: auto;
    ${({ isDrawerOpen }) => isDrawerOpen && 'opacity: 0;'}
  }
  ${mediaQueries.xLarge} {
    display: none;
  }
`

export const Backdrop = styled(Box)<{ offset: number }>`
  position: fixed;
  left: 0;
  top: ${({ offset }) => offset}px;
  background: rgba(53, 53, 53, 0.2);
  width: 100vw;
  height: 100vh;
  z-index: ${theme.layers.indexOf('backdrop')};
`

export const Drawer = styled(Box)<{ isOpen: boolean; offset: number }>`
  position: fixed;
  top: ${({ offset }) => offset}px;
  right: 0;
  height: ${({ offset }) => `calc(100vh - ${offset}px)`};
  width: 240px;
  transition: all 0.3s ease;
  transform: translate(${({ isOpen }) => (isOpen ? '0, 0' : '100%, 0')});
  z-index: ${theme.layers.indexOf('drawer')};
  background: #fff;
  ${({ isOpen }) => isOpen && 'box-shadow: 0 0 40px 10px rgba(0, 0, 0, .4);'}
  display: flex;
  flex-direction: column;
  ${mediaQueries.medium} {
    visibility: ${({ isOpen }) => (isOpen ? 'visible' : 'hidden')};
    opacity: ${({ isOpen }) => Number(isOpen)};
    top: ${({ offset, isOpen }) => (isOpen ? offset : offset + 50)}px;
    height: auto;
    transform: translate(${({ isOpen }) => (isOpen ? '0, 0' : '0, 0')});
    width: 100%;
    left: 0;
    padding: 0;
  }
`
