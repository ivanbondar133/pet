import React, { Dispatch, FC, SetStateAction, useEffect, useState } from 'react'
import { Box, Container } from '../../helpers'
import theme from 'theme'
import { Flex } from '../../helpers'
import Portal from 'components/Portal'
import Link from 'next/link'
import useResizeObserver from 'use-resize-observer'
import { MenuContext } from 'contexts/MenuContext'
import { MenuWrap, MenuLink, Drawer, HamBtn, Backdrop } from './styles'
import { Routes } from 'shared/routes'

export const MenuList: FC<{ close?: Dispatch<SetStateAction<false>> }> = ({ close }) => {
  return (
    <nav>
      <Flex as="ul" flexDirection={['column', 'row']} ml={[-12, -12, -12, 48]}>
        <li>
          <MenuLink href="/#consultation" onClick={() => close && close(false)}>
            Консультация
          </MenuLink>
        </li>
        <li>
          <MenuLink href="/#about" onClick={() => close && close(false)}>
            О компании
          </MenuLink>
        </li>
        <li>
          <MenuLink href="/#pricing" onClick={() => close && close(false)}>
            Тарифы
          </MenuLink>
        </li>
        <li>
          <Link href={Routes.SHARES + '/1'}>
            <MenuLink onClick={() => close && close(false)}>Акции</MenuLink>
          </Link>
        </li>
        <li>
          <Link href={Routes.CATALOG}>
            <MenuLink onClick={() => close && close(false)}>Каталог</MenuLink>
          </Link>
        </li>
        <li>
          <Link href={Routes.NEWS}>
            <MenuLink>Новости</MenuLink>
          </Link>
        </li>
      </Flex>
    </nav>
  )
}

export default function Menu(): JSX.Element {
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)

  const { setNavHeight } = React.useContext(MenuContext)

  const { ref, height } = useResizeObserver<HTMLDivElement>()

  useEffect(() => {
    setNavHeight(height)
  }, [height])

  return (
    <>
      <Portal id="#menu-portal">
        <MenuWrap borderBottom="1px solid" borderColor={theme.colors.primary} ref={ref}>
          <Container>
            <Flex alignItems="center" height={60}>
              <Box display={['none', 'none', 'none', 'block']}>
                <MenuList />
              </Box>
              <HamBtn isDrawerOpen={isDrawerOpen} onClick={() => setIsDrawerOpen(!isDrawerOpen)}>
                <span />
              </HamBtn>
            </Flex>
          </Container>
        </MenuWrap>
      </Portal>
      <Portal id="#myportal">
        <Drawer isOpen={isDrawerOpen} offset={height}>
          <Container flex={1} width={1}>
            <Flex
              alignItems={['flex-start', 'center']}
              flex={1}
              flexDirection={['column', 'row']}
              justifyContent="space-between"
              py={[30, 3]}
            >
              <MenuList close={setIsDrawerOpen} />
            </Flex>
          </Container>
        </Drawer>
        {isDrawerOpen && <Backdrop offset={height} onClick={() => setIsDrawerOpen(false)} />}
      </Portal>
    </>
  )
}
