import React, { FC, useRef, useState } from 'react'
import styled from 'styled-components'
import { Box, Flex } from 'components/helpers'
import Icon from 'components/helpers/Icon'
import theme, { BORDER_RADIUS } from 'theme'
import useClickOutside from 'hooks/useClickOutside'
import { SortOptionsType } from 'types'
import { useUrlHelper } from 'hooks/useUrlHelper'
import check from 'public/icons/checkmark.svg'

const LineItem = styled(Box)<{ selected?: boolean }>`
  white-space: nowrap;
  transition: all 0.3s ease;
  cursor: pointer;
  line-height: 1.5;
  background-repeat: no-repeat;
  background-position: right 10px center;
  padding-right: 30px;
  &:hover {
    background-color: ${theme.colors.grayLight};
  }
  ${({ selected }) => selected && `background-image: url(${check.src})`}
`

const DropDownWrap = styled(Box)<{ place?: string }>`
  position: absolute;
  top: 100%;
  left: 0;
  border-radius: ${BORDER_RADIUS}px;
  box-shadow: 0px 7px 15px rgba(180, 180, 180, 0.25);
  padding: ${theme.space[2]} 0;
  background: white;
  ${({ place }) => place}
`

const anchors = {
  topCenter: `top: 100%; left: 50%; transform: translate(-50%, 0);`,
  topLeft: `top: 100%; left: 0; transform: translate(-50%, 0);`,
  topRight: `top: 100%; right: 0; transform: translate(-50%, 0);`,
}

const GroupWrap = styled(Box)`
  &:not(:first-child) {
    border-top: 1px solid ${theme.colors.grayLight};
  }
`

type Props = {
  options: SortOptionsType
  anchor?: 'topCenter' | 'topLeft' | 'topRight'
  bordered?: boolean
}

const Dropdown: FC<Props> = ({ bordered, options, children, anchor }) => {
  const [opened, setOpened] = useState(false)

  const ddRef = useRef()
  useClickOutside(ddRef, () => {
    setOpened(false)
  })

  const { updateQuery, query } = useUrlHelper()

  const handleSelect = (key, val) => {
    updateQuery({
      ...query,
      [key]: val,
    })
  }

  return (
    <Box border={bordered ? '1px solid' : 0} borderColor="grayLight" position="relative">
      <Flex
        onClick={(e) => {
          e.stopPropagation()
          setOpened(!opened)
        }}
        p={2}
      >
        {children} <Icon name={opened ? 'angle-up' : 'angle-down'} />
      </Flex>
      {opened && (
        <DropDownWrap
          onClick={(e) => {
            e.stopPropagation()
          }}
          place={anchors[anchor]}
          ref={ddRef}
        >
          {Object.keys(options).map((option, idx) => {
            return (
              <GroupWrap key={idx}>
                {options[option].map((item) => (
                  <LineItem
                    key={item.value}
                    onClick={() => handleSelect(option, item.value)}
                    px={3}
                    py={1}
                    selected={query[option] === item.value}
                  >
                    {item.label}
                  </LineItem>
                ))}
              </GroupWrap>
            )
          })}
        </DropDownWrap>
      )}
    </Box>
  )
}

export default Dropdown
