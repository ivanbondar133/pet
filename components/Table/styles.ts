import styled from 'styled-components'
import theme from '../../theme'
import { TableBox } from '../helpers'

export const Table = styled(TableBox)`
  border-collapse: collapse;
  tr:last-child {
    td {
      border-bottom: 0;
    }
  }
`

type Props = {
  sortable?: boolean
}

export const Th = styled.th<Props>`
  border-bottom: 1px solid ${theme.colors.grayLight};
  padding: ${theme.space[2]};
  text-align: left;
  font-size: ${theme.fontSizes[1]};
  font-weight: 400;
  ${(props) => props.sortable && `cursor: pointer; text-decoration: underline;`}
  position: relative;
`

export const Td = styled.td`
  border-bottom: 1px solid ${theme.colors.grayLight};
  padding: ${theme.space[2]};
  text-align: left;
  font-size: ${theme.fontSizes[2]};
  font-weight: 400;
`
