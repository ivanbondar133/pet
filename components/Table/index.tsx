import React, { FC } from 'react'
import { useTable } from 'react-table'
import { Table, Th, Td } from './styles'
import { AnyObject } from 'types'
import { useUrlHelper } from 'hooks/useUrlHelper'
import Icon from '../helpers/Icon'
import { AbsoluteBox } from '../helpers/AbsoluteBox'

export type TableColumn = {
  Header?: string | ((cellProps) => React.ReactNode)
  accessor: string
  sortable?: boolean
  Cell?: (cellProps) => React.ReactNode
  id: string
}
export type Column = Omit<TableColumn, 'id'>

type TableProps = {
  columns: Column[]
  data: AnyObject[]
  getCellProps?: any
  getColumnProps?: any
  getRowProps?: any
  tableStyle?: AnyObject
}
// Create a default prop getter
const defaultPropGetter = () => ({})

const AppTable: FC<TableProps> = ({
  getCellProps = defaultPropGetter,
  getColumnProps = defaultPropGetter,
  getRowProps = defaultPropGetter,
  columns,
  data,
  tableStyle,
}) => {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = useTable({ columns, data })

  const { updateQuery, query } = useUrlHelper()

  const handleSort = (column: TableColumn): void => {
    const { id } = column
    if (!column.sortable) return
    const sortKey = 'sort_' + id
    if (!query[sortKey]) {
      updateQuery({
        [sortKey]: 'asc',
      })
    } else if (query[sortKey] && query[sortKey] === 'asc') {
      updateQuery({
        [sortKey]: 'desc',
      })
    } else {
      updateQuery(sortKey)
    }
  }

  return (
    <Table {...tableStyle} {...getTableProps()}>
      <thead>
        {headerGroups.map((headerGroup, idx) => (
          <tr key={idx} {...headerGroup.getHeaderGroupProps()}>
            {headerGroup.headers.map((column) => {
              const direction = query['sort_' + column.id]
              return (
                <Th
                  key={column.id}
                  onClick={() => handleSort(column)}
                  sortable={column.sortable}
                  {...column.getHeaderProps()}
                >
                  {direction && (
                    <AbsoluteBox anchor="center-left">
                      <Icon name={'sort_' + direction} />
                    </AbsoluteBox>
                  )}
                  {column.render('Header')}
                </Th>
              )
            })}
          </tr>
        ))}
      </thead>
      <tbody {...getTableBodyProps()}>
        {rows.map((row) => {
          prepareRow(row)
          return (
            <tr key={row.id} {...row.getRowProps(getRowProps(row))}>
              {row.cells.map((cell) => {
                return (
                  <Td
                    key={cell.column.id + row.id}
                    {...cell.getCellProps([
                      {
                        className: cell.column.className,
                        style: cell.column.style,
                      },
                      getColumnProps(cell.column),
                      getCellProps(cell),
                    ])}
                  >
                    {cell.render('Cell')}
                  </Td>
                )
              })}
            </tr>
          )
        })}
      </tbody>
    </Table>
  )
}

export default AppTable
