import React, { useState, useEffect } from 'react'
import { Box, Col, Row } from '../helpers'
import Tabs, { TabItem } from '../helpers/Tabs'
import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle,
} from 'chart.js'

Chart.register(
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle
)

const periods: TabItem[] = [
  {
    label: 'День',
    value: 'day',
  },
  {
    label: 'Неделя',
    value: 'week',
  },
  {
    label: 'Месяц',
    value: 'month',
  },
  {
    label: 'Пол года',
    value: 'half_year',
  },
  {
    label: 'Год',
    value: 'year',
  },
  {
    label: 'Все время',
    value: 'all_time',
  },
]

const ChartComponent = () => {
  const [period, setPeriod] = useState('day')

  useEffect(() => {
    const canvas = document.getElementById('myChart') as HTMLCanvasElement
    const ctx = canvas.getContext('2d')
    const myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
        datasets: [
          {
            label: '# of Votes',
            data: [12, 19, 3, 5, 2, 3],
            backgroundColor: 'rgba(255, 99, 132, 0.2)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1,
          },
        ],
      },
      options: {
        scales: {
          y: {
            beginAtZero: true,
          },
        },
      },
    })
    return () => {
      myChart.destroy()
    }
  }, [])

  return (
    <Box>
      <Row>
        <Col display={['none', 'block']} width={[1 / 4]}>
          <Tabs current={period} list={periods} onChange={(sel) => setPeriod(sel.value)} size="md" vertical />
        </Col>
        <Col width={[1, 3 / 4]}>
          <canvas height="null" id="myChart" width="null" />
        </Col>
      </Row>
    </Box>
  )
}

export default React.memo(ChartComponent)
