import React, { FC, useEffect, useState } from 'react'
import { Box } from 'components/helpers'
import { useUrlHelper } from 'hooks/useUrlHelper'
import Icon from 'components/helpers/Icon'
import styled from 'styled-components'
import theme, { BORDER_RADIUS } from 'theme'
import { useDebounce } from 'use-debounce'

const Wrap = styled(Box)`
  position: relative;
  input {
    display: block;
    width: 100%;
    padding: ${theme.space[4]} 0 ${theme.space[4]} 46px;
    border: 1px solid transparent;
    border-radius: ${BORDER_RADIUS}px;
    background: ${theme.colors.pink};
    outline: none !important;
    box-shadow: none !important;
    font-family: Montserrat, sans-serif;
    font-size: ${theme.fontSizes[1]};
    transition: all 0.3s ease;
    &:focus,
    &:hover {
      border-color: ${theme.colors.primary};
    }
  }
`

type Props = {
  placeholder?: string
}

const Search: FC<Props> = ({ placeholder = 'Поиск' }) => {
  const { updateQuery, query } = useUrlHelper()

  const [val, setVal] = useState('')
  const [value] = useDebounce(val, 500)

  useEffect(() => {
    if (query.search || value) {
      updateQuery({
        search: value,
      })
    }
  }, [value])

  useEffect(() => {
    if (query.search && query.search !== val) {
      setVal(query.search)
    }
  }, [query])

  return (
    <Wrap>
      <Icon left="15px" name="search" position="absolute" top="16px" />
      <input onChange={(e) => setVal(e.target.value)} placeholder={placeholder} type="text" value={val} />
    </Wrap>
  )
}

export default Search
