import React from 'react'
import styled from 'styled-components'
import { Box, BoxProps } from '../../helpers'
import InputMask from 'react-input-mask'

type TProps = {
  value: string
  onChange: (e: React.FormEvent<HTMLInputElement>) => void
  placeholder: string
  required?: boolean
  black?: boolean
  mask?: string
  id?: string
} & BoxProps

const Input = styled.input<{ black?: boolean }>`
  background: rgba(255, 255, 255, 0.1);
  border: 1px solid ${({ black }) => (black ? '#B9B9B9' : 'rgba(255, 255, 255, 0.5)')};
  box-sizing: border-box;
  backdrop-filter: blur(100px);
  border-radius: 5px;
  font-weight: 500;
  font-size: 13px;
  line-height: 140%;
  color: ${({ black }) => (black ? '#323232' : '#fff')};
  padding: 16px 10px;
  display: block;
  width: 100%;
  &:focus {
    border: 1px solid ${({ theme }) => theme.colors.primary};
    outline: none !important;
  }
  ::placeholder {
    color: ${({ black }) => (black ? '#323232' : '#fff')};
  }
`

export const BaseInput = ({ onChange, value, placeholder, required, mask, id, black }: TProps) => {
  return (
    <Box flex={1}>
      {mask ? (
        <InputMask mask={mask} onChange={onChange} placeholder={placeholder} value={value}>
          {(inpProps: never) => <Input black={black} {...inpProps} />}
        </InputMask>
      ) : (
        <Input black={black} id={id} onChange={onChange} placeholder={placeholder} required={required} value={value} />
      )}
    </Box>
  )
}
