import { AnyObject } from 'types'
import theme, { BORDER_RADIUS } from 'theme'
import styled from 'styled-components'
import { Option } from './index'

export const customStyles = (hasPlaceholder: boolean): AnyObject => ({
  option: (provided: AnyObject, state: AnyObject): AnyObject => ({
    ...provided,
    padding: '9px 11px',
    color: theme.colors.grayDark,
    background: state.isSelected ? theme.colors.grayLighter : 'transparent',
    '&:hover': {
      cursor: 'pointer',
      background: theme.colors.grayLighter,
    },
  }),
  placeholder: (provided: AnyObject, state: AnyObject): AnyObject => {
    const fz = state.hasValue ? 13 : 16
    const color = state.hasValue ? theme.colors.grayLight : theme.colors.gray
    return {
      ...provided,
      transition: 'all 0.3s ease',
      color,
      transform: state.hasValue ? 'translateY(-10px)' : null,
      fontSize: fz,
    }
  },
  control: (provided: AnyObject): AnyObject => {
    return {
      ...provided,
      border: `1px solid ${theme.colors.primaryLight}`,
      borderRadius: BORDER_RADIUS,
      height: hasPlaceholder ? 48 : 40,
      boxShadow: 'none !important',
      width: '100%',
      '&:hover': {
        border: `1px solid ${theme.colors.primary}`,
      },
    }
  },
  singleValue: (provided: AnyObject, state: AnyObject): AnyObject => {
    return {
      ...provided,
      transition: 'all 0.3s ease',
      transform: state.hasValue && hasPlaceholder ? 'translateY(8px)' : null,
    }
  },
  input: (provided: AnyObject, state: AnyObject): AnyObject => {
    return {
      ...provided,
      transform: state.hasValue && hasPlaceholder ? 'translateY(8px)' : null,
    }
  },
  indicatorSeparator: (): AnyObject => {
    return {
      display: 'none !important',
    }
  },
  valueContainer: (provided: AnyObject): AnyObject => {
    return {
      ...provided,
      paddingTop: 0,
      paddingBottom: 0,
      height: '100%',
    }
  },
  menu: (provided: AnyObject): AnyObject => {
    return {
      ...provided,
      border: `0.5px solid ${theme.colors.primaryLight}`,
      boxSizing: 'border-box',
      boxShadow: '0px 7px 15px rgba(180, 180, 180, 0.25)',
      borderRadius: BORDER_RADIUS,
      overflow: 'hidden',
      padding: '10px 0',
      width: '100%',
    }
  },
})

export const HTMLSelectContainer = styled.div<{ error?: boolean }>`
  position: relative;
  width: 100%;
`

export const HTMLSelect = styled.select<{ error?: boolean; options: Option[] }>`
  width: 100%;
  max-height: 260px;
  padding: 0 ${theme.space[4]};
  line-height: 43px;
  color: ${({ theme, error }) => (error ? theme.colors.red : theme.colors.black)};
  border: 1px solid ${({ theme, error }) => (error ? theme.colors.red : theme.colors.black)};
  border-radius: 0;
  background: transparent;
  font-family: ${({ theme }) => theme.fonts.paragraph};
  font-size: ${theme.fontSizes[8]};
  webkit-font-smoothing: antialiased;
  :focus {
    outline: none;
  }
  background: url('data:image/svg+xml;base64, PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjAiIHdpZHRoPSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIiBhcmlhLWhpZGRlbj0idHJ1ZSIgZm9jdXNhYmxlPSJmYWxzZSIgY2xhc3M9ImNzcy10ajViZGUtU3ZnIj48cGF0aCBkPSJNNC41MTYgNy41NDhjMC40MzYtMC40NDYgMS4wNDMtMC40ODEgMS41NzYgMGwzLjkwOCAzLjc0NyAzLjkwOC0zLjc0N2MwLjUzMy0wLjQ4MSAxLjE0MS0wLjQ0NiAxLjU3NCAwIDAuNDM2IDAuNDQ1IDAuNDA4IDEuMTk3IDAgMS42MTUtMC40MDYgMC40MTgtNC42OTUgNC41MDItNC42OTUgNC41MDItMC4yMTcgMC4yMjMtMC41MDIgMC4zMzUtMC43ODcgMC4zMzVzLTAuNTctMC4xMTItMC43ODktMC4zMzVjMCAwLTQuMjg3LTQuMDg0LTQuNjk1LTQuNTAycy0wLjQzNi0xLjE3IDAtMS42MTV6Ij48L3BhdGg+PC9zdmc+')
    no-repeat 95% 50%;
  -moz-appearance: none;
  -webkit-appearance: none;
  appearance: none;
  &::-ms-expand {
    display: none;
  }
  /* Removes default arrow from firefox */
  text-overflow: '';
  text-indent: 0.01px;
  /* Empty value instead of a placeholder for <select> */
  option[value=''][disabled] {
    display: none;
  }
`
