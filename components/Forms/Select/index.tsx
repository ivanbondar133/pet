import { Box } from 'components/helpers'
import React, { FC } from 'react'
import Select, { Props, components, ValueContainerProps, PlaceholderProps, DropdownIndicatorProps } from 'react-select'
import { customStyles, HTMLSelect, HTMLSelectContainer } from './styles'
import { TabItem } from '../../helpers/Tabs'
import Icon from '../../helpers/Icon'
import { isMobile } from 'react-device-detect'
import { AnyObject } from 'types'

export type Option = TabItem
type SelectProps = {
  options: Option[]
  name?: string
  nativeSelectOnMobile?: boolean
  disabled?: boolean
  error?: boolean
  onChange: (arg: AnyObject | null) => void
  placeholder?: string | boolean
  value?: string | number | string[] | undefined | Option
  multiple?: boolean
  isMulti?: boolean
} & Props

const DropdownIndicator = (props: DropdownIndicatorProps) => {
  return (
    <components.DropdownIndicator {...props}>
      <Icon name="angle-down" />
    </components.DropdownIndicator>
  )
}
const { ValueContainer, Placeholder } = components
const CustomValueContainer: FC<ValueContainerProps & PlaceholderProps> = ({ children, ...props }) => {
  return (
    <ValueContainer {...props}>
      <Placeholder {...props} isFocused={props.isFocused}>
        {props.selectProps.placeholder}
      </Placeholder>
      {React.Children.map(children, (child) => (child ? child : null))}
    </ValueContainer>
  )
}
// && child.type !== Placeholder 21 line
const BaseSelect: FC<SelectProps> = (props) => {
  const { value, options, disabled, onChange, error, placeholder, isMulti, nativeSelectOnMobile } = props
  let parsedValue
  if (isMobile && !nativeSelectOnMobile) {
    parsedValue = options.find((option) => option && option.value === value)
  } else if (!isMobile && isMulti) {
    if (value) {
      const multipleValues = (value as string).split(',')
      // @ts-ignore
      parsedValue = options.filter(({ value: v }) => multipleValues.includes(v))
    }
  } else if (!isMobile && !isMulti) {
    parsedValue = options.find(({ value: v }) => v === value)
  } else if (isMobile && isMulti && value === '') {
    parsedValue = []
    // } else if (isMobile && isMulti && isString(value)) {
    //   parsedValue = value.split(',')
  } else {
    parsedValue = value
  }

  return isMobile && nativeSelectOnMobile ? (
    <HTMLSelectContainer error={error}>
      <HTMLSelect
        disabled={disabled}
        error={error}
        multiple={isMulti}
        onChange={onChange}
        options={options}
        value={parsedValue as string | string[]}
      >
        <option aria-selected={false} disabled value="">
          {placeholder}
        </option>
        {options.map((option: Option) => (
          <option aria-selected key={option.value} value={option.value}>
            {option.label}
          </option>
        ))}
      </HTMLSelect>
    </HTMLSelectContainer>
  ) : (
    <Box>
      <Select
        components={{ ValueContainer: CustomValueContainer, DropdownIndicator }}
        options={options}
        styles={customStyles(Boolean(props.placeholder))}
        {...props}
      />
    </Box>
  )
}

export default BaseSelect
