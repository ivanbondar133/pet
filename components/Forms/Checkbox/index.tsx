import React from 'react'
import styled from 'styled-components'
import theme from 'theme'
import Icon from '../../helpers/Icon'

const CheckboxContainer = styled.div`
  display: inline-block;
  vertical-align: middle;
  margin-right: 5px;
  height: 16px;
`

const HiddenCheckbox = styled.input.attrs({ type: 'checkbox' })`
  border: 0;
  clip: rect(0 0 0 0);
  clippath: inset(50%);
  height: 1px;
  margin: -1px;
  overflow: hidden;
  padding: 0;
  position: absolute;
  white-space: nowrap;
  width: 1px;
`

const StyledCheckbox = styled.div<{ checked: boolean; partial?: boolean }>`
  display: inline-block;
  width: 16px;
  height: 16px;
  background: ${(props) => (props.checked ? theme.colors.primary : 'transparent')};
  border-radius: 3px;
  transition: all 150ms;
  border: 1.5px solid ${(props) => (props.checked || props.partial ? theme.colors.primary : theme.colors.grayLight)};
  img {
    transform: translate(1px, 1px);
  }
`

const Wrap = styled.div`
  display: flex;
  align-items: flex-start;
  cursor: pointer;
  span {
    user-select: none;
    line-height: 1.1;
  }
`

interface IProps {
  checked: boolean
  partial?: boolean
  children?: React.ReactNode
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void
}

const Checkbox: React.FC<IProps> = ({ checked, partial, children, ...props }) => {
  return (
    <Wrap as="label">
      <CheckboxContainer>
        <HiddenCheckbox checked={checked} {...props} />
        <StyledCheckbox checked={checked} partial={partial}>
          <Icon name={checked ? 'check' : partial ? 'line' : null} />
        </StyledCheckbox>
      </CheckboxContainer>
      <span>{children}</span>
    </Wrap>
  )
}

export default React.memo(Checkbox)
