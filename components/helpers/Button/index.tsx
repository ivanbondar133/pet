import styled from 'styled-components'
import { compose, space, SpaceProps, variant, size, SizeProps, LayoutProps, layout } from 'styled-system'
import variants from './buttonVariants'
import { AbsoluteLoader } from '../LoadingSpinner'
import { BORDER_RADIUS } from 'theme'
import React from 'react'

export type ButtonSizes = 'wide' | 'sm' | 'md' | 'lg' | 'fit'

type Props = {
  disabled?: boolean
  loading?: boolean
  children: React.ReactNode
  size?: ButtonSizes
} & SpaceProps &
  SizeProps &
  LayoutProps &
  React.ButtonHTMLAttributes<HTMLButtonElement>

const StyledButton = styled.button<Props>`
  ${compose(space, size, layout)}
  appearance: none;
  font-family: Roboto, sans-serif;
  font-size: 15px;
  line-height: 1;
  -webkit-border-radius: ${BORDER_RADIUS}px;
  -moz-border-radius: ${BORDER_RADIUS}px;
  border-radius: ${BORDER_RADIUS}px;
  box-sizing: border-box;
  display: inline-flex;
  align-items: center;
  text-align: center;
  justify-content: center;
  border: 1px solid transparent;
  transition: all 0.2s ease;
  text-decoration: none !important;
  position: relative;
  :active {
    span {
      transform: translateY(1px);
    }
  }
  ${({ disabled }) => `
    cursor: ${disabled ? 'normal' : 'pointer'};
  `}
  ${({ loading }) => loading && `span {opacity: 0;}`}
  ${({ disabled }) => {
    return variant({
      variants: variants(disabled),
    })
  }}
  ${(props) => {
    switch (props.size) {
      case 'sm':
        return `padding: ${props.theme.space[1]} ${props.theme.space[5]};`
      case 'md':
        return `padding: ${props.theme.space[2]} ${props.theme.space[6]};`
      case 'wide':
        return `padding: ${props.theme.space[2]} ${props.theme.space[8]};`
      case 'fit':
        return `
        width: 100%;
        padding: ${props.theme.space[4]} 0;
        `
      case 'lg':
      default:
        return `padding: ${props.theme.space[4]} ${props.theme.space[6]};`
    }
  }}
`

const Button = ({ children, ...props }: Props): JSX.Element => {
  return (
    <StyledButton {...props}>
      {props.loading && <AbsoluteLoader />}
      <span>{children}</span>
    </StyledButton>
  )
}

Button.defaultProps = {
  variant: 'primary',
}

export default Button
