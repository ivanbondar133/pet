import React from 'react'
import { select, text, boolean } from '@storybook/addon-knobs'
import _Button from '.'
import variants from './buttonVariants'

export default {
  title: 'Base/Button',
}

const btnVariants = Object.keys(variants(true))

export const Button = (): JSX.Element => (
  <_Button
    disabled={boolean('Disabled', false)}
    loading={boolean('Loading', false)}
    //@ts-ignore
    size={select('Size', ['sm', 'md', 'lg', 'fit', 'nopadding'], 'lg')}
    variant={select('Variant', btnVariants, btnVariants[0])}
  >
    {text('Button Label', 'Button Label')}
  </_Button>
)

// Button.parameters = {
// 	jest: ["Buttons.test.js"],
// }
