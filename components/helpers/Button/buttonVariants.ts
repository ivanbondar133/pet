export default (disabled) => ({
  primary: {
    color: 'white',
    bg: disabled ? 'primaryLight' : 'primary',
    '&:hover': {
      bg: disabled ? 'primaryLight' : 'primaryDark',
    },
  },
  secondary: {
    color: 'black',
    bg: 'transparent',
    border: '1px solid',
    borderColor: disabled ? 'primaryLight' : 'primary',
    '&:hover': {
      bg: disabled ? 'transparent' : 'primaryDark',
      color: disabled ? 'gray' : 'white',
      border: '1px solid',
      borderColor: disabled ? 'primaryLight' : 'primaryDark',
    },
  },
  accent: {
    color: 'white',
    bg: 'accent',
    border: '1px solid',
    borderColor: disabled ? 'accentLight' : 'accent',
    '&:hover': {
      bg: disabled ? 'transparent' : 'accentDark',
      color: disabled ? 'grayDark' : 'white',
      border: '1px solid',
      borderColor: disabled ? 'accentLight' : 'accentDark',
    },
  },
  outlined: {
    color: disabled ? 'gray' : 'black',
    bg: 'transparent',
    border: '1px solid',
    borderColor: disabled ? 'primaryLight' : 'primary',
    '&:hover': {
      boxShadow: !disabled && '0px 0px 0px 1px #FF1167',
      borderColor: disabled ? 'primaryLight' : 'primary',
    },
  },
  outlinedSecondary: {
    color: disabled ? 'gray' : 'black',
    bg: 'transparent',
    border: '1px solid',
    borderColor: 'primaryLight',
    '&:hover': {
      color: disabled ? 'gray' : 'white',
      bg: disabled ? 'transparent' : 'primary',
      boxShadow: !disabled && '0px 0px 0px 1px #FF1167',
      borderColor: disabled ? 'primaryLight' : 'primary',
    },
  },
})
