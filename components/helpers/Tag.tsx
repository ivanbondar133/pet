import { Box } from './Box'
import styled from 'styled-components'
import theme, { BORDER_RADIUS, colors } from '../../theme'
import { variant } from 'styled-system'

const tagVariants = {}
for (const key in colors) {
  tagVariants[key] = {
    backgroundColor: colors[key],
  }
}

export const Tag = styled(Box)<{ variant: string }>`
  border-radius: ${BORDER_RADIUS}px;
  font-size: ${theme.fontSizes[0]};
  padding: ${theme.space[0]} ${theme.space[2]};
  ${variant({
    variants: tagVariants,
  })}
`
