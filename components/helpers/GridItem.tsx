import styled from 'styled-components'

import { Box, BoxProps } from '../helpers'

export type ColProps = {
  span?: Array<number>
  offset?: Array<number>
  fit?: boolean
} & BoxProps

/**
 * Col is a single column of the grid.
 * Items can span multiple columns.
 * Large Desktop: 12 columns
 * Desktop: 12 columns
 * Tablet: 4 columns
 * Mobile: 4 columns
 *
 * @param span The number of columns the element spans, i.e. span={[4, 3, 8, 10]}
 * @param offset The offset of columns from the left, i.e. offset={[0, 1, 2, 1]}
 */
export const GridItem = styled(Box)<ColProps>`
  box-sizing: border-box;
  grid-column-end: span ${(props) => props.span![0]};
  @media screen and (min-width: ${(props) => props.theme.breakpoints[0]}) {
    grid-column-end: span ${(props) => props.span![1]};
  }
  @media screen and (min-width: ${(props) => props.theme.breakpoints[1]}) {
    grid-column-end: span ${(props) => props.span![2]};
  }
  @media screen and (min-width: ${(props) => props.theme.breakpoints[2]}) {
    grid-column-end: span ${(props) => props.span![3]};
  }
  ${({ offset, theme }) =>
    offset &&
    `
    grid-column-start: ${offset![0] + 1}
    @media screen and (min-width: ${theme.breakpoints[0]}) {
      grid-column-start: ${offset![1] + 1}
    }
    @media screen and (min-width: ${theme.breakpoints[1]}) {
      grid-column-start: ${offset![2] + 1}
    }
    @media screen and (min-width: ${theme.breakpoints[2]}) {
      grid-column-start: ${offset![3] + 1}
    }
  `}
  ${({ fit }) => fit && `grid-column: 1/-1 !important;`}
`

GridItem.defaultProps = {
  span: [1, 1, 1, 1],
}
