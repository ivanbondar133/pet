import { flexbox, FlexboxProps } from 'styled-system'
import styled from 'styled-components'
import { Box, BoxProps } from './Box'

export type TFlexProps = BoxProps & FlexboxProps

export const Flex = styled(Box)<TFlexProps>`
  display: flex;
  ${flexbox}
`

export const FlexCenter = styled(Flex)`
  align-items: center;
  justify-content: center;
`

export const FlexBetween = styled(Flex)`
  justify-content: space-between;
`

export const FlexColumn = styled(Flex)`
  flex-direction: column;
`
