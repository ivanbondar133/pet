import { motion } from 'framer-motion'
import React, { FC } from 'react'
import useScroll from '../../hooks/useScroll'
import { TAnimation } from '../../utils/animation'

export type TProps = {
  variants?: TAnimation
  style?: any
}

const Animated: FC<TProps> = ({ children, variants, style }): JSX.Element => {
  const [element, animControls] = useScroll()

  return (
    <motion.div animate={animControls} initial="hidden" ref={element} style={style} variants={variants}>
      {children}
    </motion.div>
  )
}

export default Animated
