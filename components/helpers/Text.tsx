import styled from 'styled-components'
import {
  compose,
  variant,
  layout,
  typography,
  space,
  color,
  SpaceProps,
  TypographyProps,
  ColorProps,
  LayoutProps,
} from 'styled-system'
import theme, { FontConfigurations } from 'theme'
import { AnyObject } from 'types'
import { getTextVariant } from 'utils/getTextVariant'
import React from 'react'

export enum TextVariants {
  PRIMARY = 'primary',
  SUCCESS = 'success',
  ERROR = 'error',
  GRAY_DARK = 'grayDark',
}

type TTextProps = TypographyProps &
  ColorProps &
  LayoutProps &
  SpaceProps & {
    as?: string
    variant?: string
    bold?: boolean
    semi?: boolean
    medium?: boolean
  }

export const Text = styled('div')<TTextProps>`
  ${({ theme }) => {
    return variant({
      variants: {
        primary: {
          color: theme.colors[TextVariants.PRIMARY],
        },
        success: {
          color: theme.colors[TextVariants.SUCCESS],
        },
        error: {
          color: theme.colors[TextVariants.ERROR],
        },
        grayDark: {
          color: theme.colors[TextVariants.GRAY_DARK],
        },
      },
    })
  }}
  ${compose(typography, space, color, layout)}
  ${({ bold }) => (bold ? 'font-weight: 700;' : null)}
  ${({ semi }) => (semi ? 'font-weight: 600;' : null)}
  ${({ medium }) => (medium ? 'font-weight: 500;' : null)}
`

export const PreHeader = styled.div`
  font-family: Montserrat, sans-serif;
  font-style: normal;
  font-weight: 500;
  font-size: ${theme.fontSizes[1]};
  line-height: 17px;
  color: ${({ color }) => color || theme.colors.gray};
`

export const H1 = ({ children, ...props }: AnyObject): JSX.Element => {
  return (
    <Text as="h1" {...FontConfigurations.h1} {...props}>
      {children}
    </Text>
  )
}

export const H2 = ({ children, ...props }: AnyObject): JSX.Element => {
  return (
    <Text as="h2" {...FontConfigurations.h2} {...props}>
      {children}
    </Text>
  )
}

export const Paragraph = ({ children, ...props }: AnyObject): JSX.Element => {
  return (
    <Text as="p" {...FontConfigurations.p} {...props}>
      {children}
    </Text>
  )
}

export const H4 = ({ children, ...props }: AnyObject): JSX.Element => {
  return (
    <Text as="h4" {...FontConfigurations.h4} {...props}>
      {children}
    </Text>
  )
}

export const H3 = ({ children, ...props }: AnyObject): JSX.Element => {
  return (
    <Text as="h3" {...FontConfigurations.h3} {...props}>
      {children}
    </Text>
  )
}

const DeltaWrap = styled(Text)`
  ${({ variant }) => variant !== TextVariants.GRAY_DARK && `padding-left: 14px;`}
  position: relative;
  display: inline-block;
  &:before {
    content: '';
    position: absolute;
    top: 50%;
    left: 0;
    transform: translateY(-50%);
    ${({ variant }) =>
      variant === TextVariants.SUCCESS &&
      `
      width: 0;
      height: 0;
      border-left: 5px solid transparent;
      border-right: 5px solid transparent;
      border-bottom: 5px solid ${theme.colors[TextVariants.SUCCESS]};
   `}
    ${({ variant }) =>
      variant === TextVariants.ERROR &&
      `
      width: 0;
      height: 0;
      border-left: 5px solid transparent;
      border-right: 5px solid transparent;
      border-top: 5px solid ${theme.colors[TextVariants.ERROR]};
   `}
  }
`

export type DeltaProps = {
  percent?: number | undefined
  value?: number
} & TypographyProps &
  SpaceProps

export const Delta = ({ percent, value, ...rest }: DeltaProps): JSX.Element => {
  if (!value && !percent) return null
  if (percent && !value) {
    return (
      <DeltaWrap variant={getTextVariant(percent)} {...rest}>
        {percent}%
      </DeltaWrap>
    )
  }
  return (
    <DeltaWrap variant={getTextVariant(value)} {...rest}>
      {value}₽ {percent && `(${percent}%)`}
    </DeltaWrap>
  )
}

export const PlainLink = styled.a`
  color: ${theme.colors.primary}
  text-decoration: underline;
  :hover {
    color: ${theme.colors.primaryDark}
  }
`
