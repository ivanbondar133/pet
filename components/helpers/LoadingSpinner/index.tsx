import React from "react"
import styled from "styled-components"

import { Box } from "../Box"
import { AbsoluteBox } from "../AbsoluteBox"

export const Loader = styled(Box)`
  border-radius: 50%;
  width: 20px;
  height: 20px;
  position: relative;
  border-top: 2px solid #ccc;
  border-right: 2px solid #ccc;
  border-bottom: 2px solid #ccc;
  border-left: 2px solid #000;
  -webkit-transform: translateZ(0);
  -ms-transform: translateZ(0);
  transform: translateZ(0);
  -webkit-animation: load 1.1s infinite linear;
  animation: load 1s infinite linear;
  @-webkit-keyframes load {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
  @keyframes load {
    0% {
      -webkit-transform: rotate(0deg);
      transform: rotate(0deg);
    }
    100% {
      -webkit-transform: rotate(360deg);
      transform: rotate(360deg);
    }
  }
`

export const AbsoluteLoader = (): JSX.Element => {
  return (
    <AbsoluteBox anchor="center-center">
      <Loader />
    </AbsoluteBox>
  )
}
