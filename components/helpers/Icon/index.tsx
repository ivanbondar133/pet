/* eslint-disable @next/next/no-img-element */
import React from 'react'
import { Flex } from 'components/helpers'
import { PositionProps } from 'styled-system'

type IconProps = {
  name: string
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void
} & PositionProps

const Icon: React.FunctionComponent<IconProps> = ({ name, ...props }): JSX.Element => {
  if (!name) return null
  return (
    <Flex alignItems="center" justifyContent="center" {...props}>
      <img alt={name} src={`/icons/${name}.svg`} />
    </Flex>
  )
}

export default Icon
