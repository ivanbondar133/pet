import React from 'react'
import { Flex } from './Flex'
import { Text } from 'components/helpers'
import { useRouter } from 'next/router'
import styled from 'styled-components'

const Wrap = styled(Flex)`
  cursor: pointer;
`

const BackBtn = () => {
  const router = useRouter()
  return (
    <Wrap onClick={() => router.back()}>
      <img alt="" src="/icons/angle-left.svg" />
      <Text fontSize={1} ml={1}>
        Назад
      </Text>
    </Wrap>
  )
}

export default BackBtn
