import { BoxProps, Box } from '../Box'
import styled, { css } from 'styled-components'
import { position, PositionProps } from 'styled-system'

type AbsoluteBoxProps = {
  anchor?:
    | 'top-center'
    | 'top-right'
    | 'center'
    | 'center-center'
    | 'center-top-right'
    | 'center-top-left'
    | 'center-right'
    | 'center-bottom'
    | 'center-bottom-right'
    | 'center-bottom-left'
    | 'center-center-left'
    | 'center-left'
    | 'bottom-right'
    | 'bottom-center-left'
    | 'right'
} & BoxProps &
  PositionProps

export const AbsoluteBox = styled(Box)<AbsoluteBoxProps>`
  position: absolute;
  ${position}
  ${(props) => {
    switch (props.anchor) {
      case 'center':
        return css`
          transform: translate(-50%, 0);
        `
      case 'center-left':
        return css`
          transform: translate(-50%, -50%);
          left: 0;
          top: 50%;
        `
      case 'center-bottom':
        return css`
          transform: translate(-50%, 50%);
          bottom: 0;
        `
      case 'center-right':
        return css`
          right: 0;
          transform: translate(50%, 0);
        `
      case 'center-center':
        return css`
          top: 50%;
          left: 50%;
          transform: translate(-50%, -50%);
        `
      case 'center-center-left':
        return css`
          top: 50%;
          left: 0;
          transform: translate(-50%, -50%);
        `
      case 'center-top-left':
        return css`
          top: 0;
          left: 0;
          transform: translate(-50%, -50%);
        `
      case 'center-bottom-left':
        return css`
          bottom: 0;
          left: 0;
          transform: translate(-50%, 50%);
        `
      case 'center-bottom-right':
        return css`
          bottom: 0;
          right: 0;
          transform: translate(50%, 50%);
        `
      case 'center-top-right':
        return css`
          top: 0;
          right: 0;
          transform: translate(50%, 50%);
        `
      case 'top-center':
        return css`
          top: 0;
          left: 50%;
          transform: translate(-50%, -50%);
        `
      case 'bottom-right':
        return css`
          bottom: 0;
          right: 0;
        `
      case 'bottom-center-left':
        return css`
          bottom: 0;
          transform: translate(0, 50%);
        `
      case 'top-right':
        return css`
          top: 0;
          right: 0;
        `
      case 'right':
        return css`
          right: 0;
        `
      default:
        return css``
    }
  }}
`
