import { Box, BoxProps } from './Box'
import { Flex } from './Flex'
import styled from 'styled-components'

import { GUTTER, maxWidths, mediaQueries } from 'theme'
import { ThemeProps } from 'theme/global-styles'
import React, { FC } from 'react'

type ContainerProps = {
  noPadding?: boolean
  fullBleed?: boolean
} & ThemeProps

export const Container = styled(Box)<ContainerProps>`
  position: relative;
  padding-left: ${GUTTER}px;
  padding-right: ${GUTTER}px;
  margin-left: auto;
  margin-right: auto;
  max-width: ${maxWidths[0]};
  display: flex;
  flex-direction: column;
  ${({ fullBleed }) =>
    !fullBleed &&
    `
    ${mediaQueries.medium} {
      max-width: ${maxWidths[1]}px;
    }
    ${mediaQueries.large} {
      max-width: ${maxWidths[2]}px;
    }
    ${mediaQueries.xLarge} {
      max-width: ${maxWidths[3]}px;
    }
  `}
  ${({ noPadding }) => noPadding && 'padding-left: 0 !important; padding-right: 0 !important;'}
`

type ColProps = {
  sticky?: boolean
} & ThemeProps

export const Row = styled(Flex)<ThemeProps>`
  flex-wrap: wrap;
  margin-left: -${GUTTER}px;
  margin-right: -${GUTTER}px;
`

export const Col = styled(Box)<ColProps>`
  padding-left: ${GUTTER}px;
  padding-right: ${GUTTER}px;
  ${({ sticky }) =>
    sticky &&
    `
    position: sticky;
    top: 61px
  `}
`

type DefaultGridProps = {
  children: React.ReactNode
  noGutter?: boolean
  fullBleed?: boolean
  noPadding?: boolean
} & BoxProps

export const DefaultGrid = styled(Box)<DefaultGridProps>`
  box-sizing: border-box;
  display: grid;
  grid: auto-flow / repeat(4, 1fr);
  grid-column-gap: ${({ noGutter }) => (noGutter ? 0 : GUTTER * 2)}px;
  ${mediaQueries.large} {
    grid: 1fr / repeat(12, 1fr);
  }
`

export const Grid: FC<any> = ({ children, fullBleed, noPadding, noGutter, ...props }): JSX.Element => {
  return (
    <Container fullBleed={fullBleed} noPadding={noPadding} {...props}>
      <DefaultGrid noGutter={noGutter}>{children}</DefaultGrid>
    </Container>
  )
}
