import React, { FC } from 'react'
import Button, { ButtonSizes } from '../Button'
import styled from 'styled-components'
import { Box } from '../Box'
import { SpaceProps } from 'styled-system'
import { useUrlHelper } from 'hooks/useUrlHelper'
import { isMobileOnly } from 'react-device-detect'
import Select from 'components/Forms/Select'
import { mediaQueries } from 'theme'

export type TabItem = {
  label: string | React.ReactNode
  value: string | number
}

export type TabsProps = {
  list: TabItem[]
  onChange?: any
  size?: ButtonSizes
  current?: number | string
  vertical?: boolean
  shouldUpdateQuery?: boolean
  queryProp?: string
} & SpaceProps

const Wrap = styled(Box)`
  ${mediaQueries.medium} {
    display: flex;
  }
  button {
    border-radius: 0;
    &:first-child {
      border-radius: 10px 0 0 10px;
    }
    &:last-child {
      border-radius: 0 10px 10px 0;
    }
  }
`

const VerticalWrap = styled(Box)`
  button {
    width: 100%;
  }
`

const Tabs: FC<TabsProps> = ({
  list,
  onChange,
  size,
  current,
  vertical,
  shouldUpdateQuery,
  queryProp = 'activeTab',
  ...props
}) => {
  const { updateQuery } = useUrlHelper()

  const [selected, setSelected] = React.useState<null | number | string>(current)

  const handleClick = (item) => {
    setSelected(item.value)
    onChange && onChange(item.value)
    if (shouldUpdateQuery) {
      updateQuery({ [queryProp]: item.value })
    }
  }

  if (vertical) {
    return (
      <VerticalWrap {...props}>
        {list.map((item) => (
          <Button
            key={item.value}
            mb={3}
            onClick={() => handleClick(item)}
            size={size}
            variant={selected === item.value ? 'primary' : 'outlinedSecondary'}
          >
            {item.label}
          </Button>
        ))}
      </VerticalWrap>
    )
  }

  if (isMobileOnly) {
    return (
      <Select
        isSearchable={false}
        onChange={(val) => handleClick(val)}
        options={list}
        placeholder={false}
        value={list.find((item) => selected === item.value)}
      />
    )
  }

  return (
    <Wrap {...props}>
      {list.map((item) => (
        <Button
          key={item.value}
          onClick={() => handleClick(item)}
          size={size}
          variant={selected === item.value ? 'primary' : 'outlinedSecondary'}
        >
          {item.label}
        </Button>
      ))}
    </Wrap>
  )
}

export default Tabs
