import React, { useState } from 'react'
import { select } from '@storybook/addon-knobs'
import _Tabs from '.'

export default {
  title: 'Base/Tabs',
}

const list = [
  { label: 'First Item', value: 1 },
  { label: 'Second Item', value: 2 },
  { label: 'Third Item With Long Description', value: 3 },
  { label: 'Fourth Item', value: 4 },
]

export const Tabs = (): JSX.Element => {
  const [state, setState] = useState<null | number>(null)
  return (
    <>
      <_Tabs list={list} onChange={setState} size={select('Size', ['sm', 'md', 'lg', 'fit'], 'lg')} />
      <span>Selected Tab: {state}</span>
    </>
  )
}

// Button.parameters = {
// 	jest: ["Buttons.test.js"],
// }
