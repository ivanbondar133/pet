import {
  space,
  SpaceProps,
  layout,
  LayoutProps,
  color,
  ColorProps,
  background,
  BackgroundProps,
  border,
  BorderProps,
  position,
  PositionProps,
  ShadowProps,
  flexbox,
  FlexboxProps,
  shadow,
  typography,
  TypographyProps,
  compose,
} from 'styled-system'
import { TProps as AnimationProps } from './Animated'
import styled from 'styled-components'
import { motion } from 'framer-motion'
import theme, { BORDER_RADIUS } from 'theme'

export type BoxProps = SpaceProps &
  LayoutProps &
  Omit<ColorProps, 'color'> &
  BackgroundProps &
  BorderProps &
  PositionProps &
  ShadowProps &
  FlexboxProps &
  AnimationProps &
  TypographyProps

export const Box = styled(motion.div)<BoxProps>`
  //-webkit-transform-style: preserve-3d;
  //transform-style: preserve-3d;
  ${compose(space, layout, color, background, border, position, shadow, flexbox, typography)}
`
export const TableBox = styled.table<BoxProps>`
  ${compose(space, layout, color, background, border, position, shadow, flexbox, typography)}
`

export const BorderedBox = styled(Box)`
  border-width: 1px;
  border-style: solid;
  border-radius: ${BORDER_RADIUS}px;
`

BorderedBox.defaultProps = {
  borderColor: theme.colors.grayLight,
}
