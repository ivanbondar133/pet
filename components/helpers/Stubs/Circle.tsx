import React from 'react'
import { Box } from '../Box'

export const Circle = ({ size }: { size: number }): JSX.Element => {
  return <Box bg="gray" borderRadius="50%" height={size} width={size} />
}
