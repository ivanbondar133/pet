import React, { FC } from 'react'
import { Box, Flex, Text } from '../helpers'

export type FinRowItemType = {
  title: string
  description: string
  value: string
}

const FinRow: FC<{ item: FinRowItemType }> = ({ item }) => {
  return (
    <Box borderBottom="1px solid" borderColor="grayLight" py={3}>
      <Flex justifyContent="space-between" mb={1}>
        <Text fontSize={3} fontWeight={500}>
          {item.title}
        </Text>
        <Text fontSize={3} fontWeight={500} textAlign="right">
          {item.value}
        </Text>
      </Flex>
      <Text color="gray">{item.description}</Text>
    </Box>
  )
}

export default FinRow
