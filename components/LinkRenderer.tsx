import React from 'react'
import Link from 'next/link'

import { PlainLink } from 'components/helpers'

type LinkRendererProps = {
  href: string
  forceExternal?: boolean
  style?: Record<string, string>
  target?: string
  rel?: string
  plainLink?: boolean
}

/**
 * Takes in a `to` link and either renders an internal app Link or an external
 * `a` tag. This is because React Router is internal to the app and doesn't support
 * external redirects
 */
export const LinkRenderer: React.FunctionComponent<LinkRendererProps> = ({
  href,
  children,
  plainLink = false,
  target,
  rel,
  ...rest
}) => {
  if (plainLink)
    return (
      <Link href={href} passHref>
        <PlainLink rel={rel} target={target} {...rest}>
          {children}
        </PlainLink>
      </Link>
    )

  return (
    <Link href={href}>
      <a rel={rel} target={target} {...rest}>
        {children}
      </a>
    </Link>
  )
}
