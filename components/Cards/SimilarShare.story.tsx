import _SimilarShares from './SimilarShares'
import { FC } from 'react'
import { Col, Container, Row } from '../helpers'
import { similarShareStub } from '../../stubs'

export default {
  title: 'Cards',
}

export const SimilarShares: FC = () => (
  <Container>
    <Row>
      <Col width={1 / 4}>
        <_SimilarShares item={similarShareStub} />
      </Col>
    </Row>
  </Container>
)
