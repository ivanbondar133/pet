import React, { FC } from 'react'
import { Box, Delta, Flex, H4, Text } from '../helpers'
import { BORDER_RADIUS, mediaQueries } from 'theme'
import { isMobile } from 'react-device-detect'
import styled from 'styled-components'
import unipro from 'public/images/unipro.svg'
import { FlexboxProps } from 'styled-system'

const Wrap = styled(Flex)`
  img {
    width: 60px;
    height: 60px;
    ${mediaQueries.large} {
      width: 100px;
      height: 100px;
    }
  }
`

const ShareCard: FC<FlexboxProps> = (props): JSX.Element => {
  const profitText = isMobile ? 'За пол года' : 'Доходность за пол года'

  return (
    <Wrap
      alignItems="center"
      bg="pink"
      borderRadius={BORDER_RADIUS}
      flexDirection={[null, null, 'column']}
      position="relative"
      px={6}
      py={5}
      textAlign={['left', 'left', 'center']}
      {...props}
    >
      <img alt="" src={unipro.src} />
      <Box ml={[2, 2, 2, 0]}>
        <H4 mt={1}>Юнипро</H4>
        <Text fontSize={1} mb={2} medium>
          UPRO
        </Text>
      </Box>
      <Box display={['none', 'none', 'block']}>
        <Text color="gray" fontSize={0} medium>
          Сектор
        </Text>
        <Text fontSize={3} mb={3} medium>
          Электроэнергетика
        </Text>
      </Box>
      <Box ml={['auto', 'auto', 0]} textAlign={['right', 'right', 'center']}>
        <Text fontSize={1} mb={1}>
          {profitText}
        </Text>
        <Delta percent={2.3} />
      </Box>
    </Wrap>
  )
}

export default ShareCard
