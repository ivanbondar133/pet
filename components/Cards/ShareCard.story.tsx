import _ShareCard from './ShareCard'
import { FC } from 'react'
import { Col, Container, Row } from '../helpers'

export default {
  title: 'Cards',
}

export const ShareCard: FC = () => (
  <Container>
    <Row>
      <Col width={1 / 4}>
        <_ShareCard />
      </Col>
    </Row>
  </Container>
)
