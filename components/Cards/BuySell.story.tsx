import _BuySell from './BuySell'
import { FC } from 'react'
import { Col, Container, Row } from '../helpers'

export default {
  title: 'Cards',
}

export const BuySell: FC = () => (
  <Container>
    <Row>
      <Col width={1 / 4}>
        <_BuySell />
      </Col>
    </Row>
  </Container>
)
