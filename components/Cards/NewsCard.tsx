import React, { FC } from 'react'
import { Box, H4, Text, Flex, Paragraph } from '../helpers'
import { TagType } from 'types'
import theme, { BORDER_RADIUS } from 'theme'
import { Tag } from '../helpers/Tag'
import styled from 'styled-components'

export type NewsItem = {
  tag: TagType
  title: string
  text: string
  created: string
}

type Props = {
  newsItem?: NewsItem
  mb?: number
  mt?: number
}

const Wrap = styled(Box)`
  cursor: pointer;
  transition: all 0.3s ease;
  :hover {
    background: ${theme.colors.pink};
  }
`

const NewsCard: FC<Props> = ({ newsItem, ...props }) => {
  return (
    <Wrap border="1px solid" borderColor="grayLight" borderRadius={BORDER_RADIUS} p={6} {...props}>
      <Flex alignItems="flex-start" justifyContent="space-between" mb={1}>
        <H4>В 1-м квартале чистая прибыль "Юнипро" по МСФО снизилась на 15,3%</H4>
        <Tag color="white" display={['none', 'none', 'block']} variant="warning">
          Новость
        </Tag>
      </Flex>

      <Paragraph fontSize={1}>
        FINMARKET.RU - По итогам 1-го квартала т.г. чистая прибыль ПАО "Юнипро" по МСФО снизилась по сравнению с
        аналогичным периодом прошлого года на 15,3% - до 4,04 млрд руб., следует из отчета компании. Прогноз EBITDA
        обновлен с учетом оплаты мощности третьего энергоблока Березовской ГРЭС с мая 2021 г., говорится в презентации.
        В состав компании входят пять тепловых электрических станций: Сургутская ГРЭС-2, Березовская ГРЭС, Шатурская
        ГРЭС, Смоленская ГРЭС и Яйвинская ГРЭС.
      </Paragraph>
      <Flex justifyContent="space-between" mt={1}>
        <Text fontSize={0}>24.01.2021</Text>
        <Tag display={[null, null, 'none']} variant="accentLight">
          Новость
        </Tag>
      </Flex>
    </Wrap>
  )
}

export default NewsCard
