import _NewsCard from './NewsCard'
import { FC } from 'react'
import { Col, Container, Row } from '../helpers'

export default {
  title: 'Cards',
}

export const NewsCard: FC = () => (
  <Container>
    <Row>
      <Col width={3 / 4}>
        <_NewsCard />
      </Col>
    </Row>
  </Container>
)
