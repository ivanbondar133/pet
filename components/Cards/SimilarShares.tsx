import React, { FC } from 'react'
import { Box, Delta, DeltaProps, FlexBetween, Text } from '../helpers'
import { BORDER_RADIUS } from 'theme'
import styled from 'styled-components'

const Wrap = styled(FlexBetween)`
  transition: all 0.3s ease;
  &:hover {
    box-shadow: 0 7px 15px rgba(180, 180, 180, 0.25);
  }
  img {
    width: 50px;
    height: 50px;
  }
`

export type SimilarShareItemType = {
  title: string
  price: string
  icon: any
  deltaValues: DeltaProps
}

const SimilarShares: FC<{ item: SimilarShareItemType }> = ({ item }): JSX.Element => {
  return (
    <Wrap bg="cardsBg" borderRadius={BORDER_RADIUS} padding={5}>
      <Box>
        <Text mb={3} semi>
          {item.title}
        </Text>
        <Text>{item.price}₽</Text>
        <Delta fontSize={0} percent={item.deltaValues.percent} value={item.deltaValues.value} />
      </Box>
      <img alt={item.title} src={item.icon.src} />
    </Wrap>
  )
}

export default SimilarShares
