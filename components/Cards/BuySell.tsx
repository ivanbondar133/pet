import React, { FC } from 'react'
import { Box, FlexBetween, Text, Flex } from '../helpers'
import { BORDER_RADIUS, mediaQueries } from '../../theme'
import Button from '../helpers/Button'
import styled from 'styled-components'
import { FlexboxProps, SizeProps, SpaceProps } from 'styled-system'

const Wrap = styled(Box)`
  overflow: hidden;
  button {
    flex: 1;
    ${mediaQueries.small} {
      border-radius: ${BORDER_RADIUS}px;
    }
    ${mediaQueries.medium} {
      border-radius: 0;
    }
    ${mediaQueries.xLarge} {
      border-radius: ${BORDER_RADIUS}px;
    }
  }
`

type Props = SizeProps & FlexboxProps & SpaceProps

const BuySell: FC<Props> = (props): JSX.Element => {
  return (
    <Wrap bg="cardsBg" borderRadius={BORDER_RADIUS} pt={6} {...props}>
      <FlexBetween mb={5} px={5}>
        <Box>
          <Text fontSize={0} mb={1}>
            Покупка
          </Text>
          <Text medium>2,798 ₽</Text>
        </Box>
        <Box textAlign="right">
          <Text fontSize={0} mb={1}>
            Продажа
          </Text>
          <Text medium>2,798 ₽</Text>
        </Box>
      </FlexBetween>
      <Flex pb={[5, 0, 0, 5]} px={[5, 0, 0, 5]}>
        <Button size="md">Купить</Button>
        <Button ml={[1, '1px', '1px', 1]} size="md">
          Продать
        </Button>
      </Flex>
    </Wrap>
  )
}

export default BuySell
