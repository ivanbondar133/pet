import { DeltaProps } from '../components/helpers'
import { SimilarShareItemType } from '../components/Cards/SimilarShares'
import tesla from 'public/images/tesla.svg'

export const deltaStub: DeltaProps = {
  percent: 1.5,
  value: 2.245,
}

export const similarShareStub: SimilarShareItemType = {
  title: 'Мосэнерго',
  price: '2,3855',
  deltaValues: deltaStub,
  icon: tesla,
}
