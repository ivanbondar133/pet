#!/usr/bin/env bash
SECONDS=0
start=`date +%s`
s=`date +%s`
echo " **** [ Executing script deploy.sh ] **** "
echo " **** [ Start docker ] **** "
[ ! -f .env ] && touch .env
echo "**** [ make build ] **** " && \
SECONDS=0
make build
end=`date +%s`
runtime=$((end-start))
echo "**** [ Total time execution: $((($runtime / 60) % 60)) min $(($runtime % 60)) sec ] **** "
