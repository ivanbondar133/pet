import Head from 'next/head'
import Header from 'components/pages/welcome/Header/Header'
import Features from 'components/pages/welcome/Features/Features'
import Chart from 'components/pages/welcome/Chart/Chart'
import Hiw from 'components/pages/welcome/HIW/HIW'
import HowToEarn from 'components/pages/welcome/HowToEarn/HowToEarn'
import Prices from 'components/pages/welcome/Prices/Prices'
import About from 'components/pages/welcome/About/About'
import News from 'components/pages/welcome/News/News'
import Steps from 'components/pages/welcome/Steps'
import Calculator from 'components/pages/welcome/Calculator'
import useWindowWidth from 'hooks/useWindowWidth'
import { breakpointsAsInts } from 'theme'
import React from 'react'

export default function Home(): JSX.Element {
  const { width } = useWindowWidth()

  return (
    <>
      <Header />
      {width > breakpointsAsInts[1] && <Features />}
      <Chart />
      <Calculator />
      <Hiw />
      <HowToEarn />
      <Prices />
      <About />
      <News />
      <Steps />
    </>
  )
}
