import { FC } from 'react'
import { Container, Row, Col, Box, Text, H3, PreHeader, Flex } from '../components/helpers'
import { H2 } from 'components/helpers'
import Button from '../components/helpers/Button'
import BackBtn from '../components/helpers/BackBtn'
import { AnyObject } from '../types'
import AppTable, { Column } from '../components/Table'
import theme, { BORDER_RADIUS } from '../theme'
import styled from 'styled-components'

const NumElem = styled(Flex)`
  border-bottom: 1px solid ${theme.colors.grayLight};
  margin-bottom: 125px;
  align-items: center;
  span {
    font-family: Rubik, sans-serif;
    font-size: 150px;
    font-weight: 700;
    color: ${theme.colors.pink};
  }
`

const Prices: FC = () => {
  const columns: Column[] = [
    {
      Header: '',
      accessor: 'col1',
    },
    {
      Header: () => {
        return <H3 textAlign="center">Название</H3>
      },
      accessor: 'col2',
      sortable: true,
      Cell: (props) => {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        return <Text dangerouslySetInnerHTML={{ __html: props.value }} textAlign="center" />
      },
    },
    {
      Header: () => {
        return <H3 textAlign="center">Название</H3>
      },
      accessor: 'col3',
      Cell: (props) => {
        // eslint-disable-next-line @typescript-eslint/naming-convention
        return <Text dangerouslySetInnerHTML={{ __html: props.value }} textAlign="center" />
      },
    },
  ]

  const tableData: AnyObject[] = [
    {
      col1: 'Открытие и закрытие счета, пополнение или вывод средств, депозитарное обслуживание',
      col2: 'Бесплатно',
      col3: 'Бесплатно',
    },
    {
      col1: 'Открытие и закрытие счета, пополнение или вывод средств, депозитарное обслуживание',
      col2: '0 ₽',
      col3: `
        0 ₽ <br />  
        — если вы не торгуете<br />
        — если у вас есть Премиальная карта Тинькофф<br />
        — если оборот прошлого периода больше 5 млн ₽<br />
        — если портфель больше 2 млн ₽<br />
        290 ₽ в остальных случаях
      `,
    },
    {
      col1: 'Открытие и закрытие счета, пополнение или вывод средств, депозитарное обслуживание',
      col2: 'В чате <br />и по телефону',
      col3: 'Персональный менеджер <br />по инвестициям',
    },
    {
      col1: 'Открытие и закрытие счета, пополнение или вывод средств, депозитарное обслуживание',
      col2: 'В чате <br />и по телефону',
      col3: 'Персональный менеджер <br />по инвестициям',
    },
    {
      col1: 'Открытие и закрытие счета, пополнение или вывод средств, депозитарное обслуживание',
      col2: 'В чате <br />и по телефону',
      col3: 'Персональный менеджер <br />по инвестициям',
    },
    {
      col1: 'Открытие и закрытие счета, пополнение или вывод средств, депозитарное обслуживание',
      col2: 'В чате <br />и по телефону',
      col3: 'Персональный менеджер <br />по инвестициям',
    },
  ]

  return (
    <Container>
      <Box py={6}>
        <BackBtn />
      </Box>
      <Row alignItems="flex-start" mb={9}>
        <Col mb={5} sticky width={[1, 1, 1 / 4]}>
          <H2 mb={6}>Тарифы</H2>
          <Button size="wide" variant="primary">
            Начать
          </Button>
        </Col>
        <Col width={[1, 1, 3 / 4]}>
          <AppTable
            columns={columns}
            data={tableData}
            getColumnProps={() => {
              return { style: { width: '33%' } }
            }}
            tableStyle={{ bg: theme.colors.pink, borderRadius: BORDER_RADIUS }}
          />
        </Col>
      </Row>
      <Row alignItems="flex-start" mb={10}>
        <Col mb={5} sticky width={[1, 1, 5 / 12]}>
          <PreHeader>Открыть счет</PreHeader>
          <H2 mb={6}>
            Всего{' '}
            <Text as="span" variant="primary">
              3 шага
            </Text>{' '}
            <br />к инвестициям
          </H2>
          <Button size="wide" variant="primary">
            Начать
          </Button>
        </Col>
        <Col width={[1, 1, 7 / 12]}>
          <NumElem>
            <span>1</span>
            <Box ml={125}>
              <H3 mb={2}>Открыть счет онлайн</H3>
              <Text fontSize={1} maxWidth={280}>
                Для открытия брокерского счета вам потребуется паспорт и несколько минут, чтобы заполнить анкету
              </Text>
            </Box>
          </NumElem>
          <NumElem>
            <span>2</span>
            <Box ml={125}>
              <H3 mb={2}>Пополните счет</H3>
              <Text fontSize={1} maxWidth={280}>
                Внесите средства на брокерский счет удобным способом
              </Text>
            </Box>
          </NumElem>
          <NumElem>
            <span>3</span>
            <Box ml={125}>
              <H3 mb={2}>Начните торги</H3>
              <Text fontSize={1} maxWidth={280}>
                Как только заявка будет одобрена вы сможете приступить к торгам в разделе каталог
              </Text>
            </Box>
          </NumElem>
        </Col>
      </Row>
    </Container>
  )
}

export default Prices
