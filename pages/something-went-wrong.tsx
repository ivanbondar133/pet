import React from 'react'

// SHARED
import { createGetServerSideProps } from 'shared/server'

// COMPONENTS
import { Box } from 'components/helpers'
import { Paragraph } from 'components/helpers'
import { LinkRenderer } from 'components/LinkRenderer'

export const getServerSideProps = createGetServerSideProps(async (_context) => {
  return {
    props: {},
  }
})

const SomethingWentWrongPage = (): JSX.Element => (
  <>
    <Box bg="white" p={[6, 8]} textAlign="center" width="100%">
      <Box py={3}>
        <Paragraph bold fontSize={[9, null, 2]}>
          Что-то пошло не так
        </Paragraph>
      </Box>
      <Paragraph color="grayDark">
        <LinkRenderer href="/" plainLink>
          ef-broker.ru
        </LinkRenderer>
      </Paragraph>
    </Box>
  </>
)

export default SomethingWentWrongPage
