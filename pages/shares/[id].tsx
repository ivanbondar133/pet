import React, { FC } from 'react'
import { Col, Container, Row, Box } from 'components/helpers'
import BackBtn from 'components/helpers/BackBtn'
import Tabs, { TabItem } from 'components/helpers/Tabs'
import ShareCard from 'components/Cards/ShareCard'
import BuySell from 'components/Cards/BuySell'
import OverView from 'components/pages/shares/OverView'
import ShareNews from 'components/pages/shares/ShareNews'
import { createGetServerSideProps } from 'shared/server'
import { GUTTER } from 'theme'
import { useUrlHelper } from '../../hooks/useUrlHelper'

const tabItems: TabItem[] = [
  {
    value: 'overview',
    label: 'Обзор',
  },
  {
    value: 'news',
    label: 'Новости',
  },
  {
    value: 'ideas',
    label: 'Идеи',
  },
  {
    value: 'dividends',
    label: 'Дивиденды',
  },
  {
    value: 'review',
    label: 'Обзоры',
  },
]
export const getServerSideProps = createGetServerSideProps(async (context) => {
  return {
    props: { query: context.query },
  }
})

const ShareId: FC = () => {
  const { query } = useUrlHelper()
  const { activeTab = 'overview' } = query

  return (
    <Container>
      <Box py={6}>
        <BackBtn />
      </Box>
      <Row flexDirection={['column-reverse', 'row']}>
        <Col width={[1, 1, 3 / 4]}>
          <Tabs current={activeTab} list={tabItems} mb={[5, 6]} shouldUpdateQuery size="fit" />
          <Box display={['none', 'flex', 'none']} mb={5}>
            <ShareCard flex={3} />
            <BuySell flex={2} ml={GUTTER} />
          </Box>
          {activeTab === 'overview' && <OverView />}
          {activeTab === 'news' && <ShareNews />}
        </Col>
        <Col display={['block', 'none', 'block']} mb={5} width={[1, 1 / 4]}>
          <ShareCard />
          <BuySell mt={6} />
        </Col>
      </Row>
    </Container>
  )
}

export default ShareId
