import React from 'react'
import { Box, Container, Flex } from '../components/helpers'
import AppTable, { Column } from '../components/Table'
import Search from '../components/Forms/Search'
import Dropdown from '../components/Table/Components/Dropdown'
import { SortOptionsType } from '../types'

const sortOptions: SortOptionsType = {
  sort_col2: [
    { label: 'По возрастанию', value: 'asc' },
    { label: 'По убыванию', value: 'desc' },
  ],
  time: [
    { label: 'Доходность за день', value: 'day' },
    { label: 'Доходность за год', value: 'year' },
  ],
}

export default function Welcome(): JSX.Element {
  const data = [
    {
      col1: 'Hello',
      col2: 'World',
    },
    {
      col1: 'react-table',
      col2: 'rocks',
    },
    {
      col1: 'whatever',
      col2: 'you want',
    },
  ]

  const columns: Column[] = [
    {
      Header: 'Column 1',
      accessor: 'col1',
    },
    {
      Header: () => {
        return (
          <Flex alignItems="center">
            Column 2 <Dropdown options={sortOptions} />
          </Flex>
        )
      },
      accessor: 'col2',
      sortable: true,
    },
  ]

  return (
    <Container pt={5}>
      <Box mb={6}>
        <Search />
      </Box>
      <AppTable columns={columns} data={data} />
    </Container>
  )
}
