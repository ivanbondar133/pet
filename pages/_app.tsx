import type { AppProps /*, AppContext */ } from 'next/app'
import { ThemeProvider } from 'styled-components'
import theme from 'theme'
import GlobalStyle from 'theme/global-styles'
import './_app.css'
import Layout from 'components/layouts/Layout'
import Animated from 'components/helpers/Animated'
import { containerAnimation } from 'utils/animation'
import Router from 'next/router'
import withYM from 'next-ym'
import MenuProvider from 'contexts/MenuContext'

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ThemeProvider theme={theme}>
      <GlobalStyle />
      <MenuProvider>
        <Layout>
          <Animated variants={containerAnimation}>
            <Component {...pageProps} />
          </Animated>
        </Layout>
      </MenuProvider>
    </ThemeProvider>
  )
}

export default withYM(process.env.YANDEX_METRIKA, Router)(MyApp)
