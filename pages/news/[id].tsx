import { useRouter } from 'next/router'
import { Container, Text } from 'components/helpers'
import { useEffect, useState } from 'react'
import useAxios from 'hooks/useAxios'
import { FontConfigurations } from '../../theme'
import styled from 'styled-components'

const NewsBody = styled.div`
  table {
    td {
      padding: 5px;
    }
  }
  p + p {
    margin-top: 16px;
  }
`

const NewsId = () => {
  const router = useRouter()
  const { id } = router.query

  const { axios, loading } = useAxios()
  const [news, setNews] = useState<any>({})

  useEffect(() => {
    axios.get('/api/v3/finmarket/news/' + id).then(({ data }) => {
      setNews(data.mbn)
    })
  }, [])

  if (loading) {
    return <Container py={50}>Загрузка...</Container>
  }

  if (!news) {
    return <Container py={50}>Ошибка загрузки. Повторите попытку позже или обратитесь в службу поддержки</Container>
  }

  return (
    <Container py={50}>
      <Text as="h2" mb={4} {...FontConfigurations.h2}>
        {news.h}
      </Text>
      <NewsBody dangerouslySetInnerHTML={{ __html: news.c }} />
    </Container>
  )
}

export async function getServerSideProps(context) {
  return {
    props: {}, // will be passed to the page component as props
  }
}

export default NewsId
