import React, {useEffect, useState} from "react";
import {Box, PreHeader, Text, Container, Row} from "components/helpers";
import {FontConfigurations} from "theme";
import NewsCard from "components/pages/welcome/News/NewsCard";
import useAxios from "../../hooks/useAxios";
import InnerPageHeader from "../../components/partial/InnerPageHeader";

const NewsIndex = () => {
	const {axios, loading} = useAxios()
	const [news, setNews] = useState([])

	useEffect(() => {
		axios.get('/api/v3/finmarket/news/index').then(({data}) => {
			setNews(data?.mbnl?.c_nwli || [])
		})
	}, [])
	return (
		<>
			<InnerPageHeader image="images/news-bg.png">
				<PreHeader color={'white'}>СМИ</PreHeader>
				<Text color={'white'} mb={2} as={'h2'} {...FontConfigurations.h2}>
					Все новости <br/>
					нашей компании
				</Text>
			</InnerPageHeader>
			<Container py={[30, 30, 50]}>
				<Row>
					{news.map((item) => <NewsCard isNewsPage width={[1, 1/2, 1/3]} key={item.i} item={item}/>)}
				</Row>
			</Container>
		</>
	)
}

export default NewsIndex
