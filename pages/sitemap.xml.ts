import React from 'react'
import fs from 'fs'

export const getServerSideProps = ({ res }) => {
  const baseUrl = {
    development: 'http://localhost:2401',
    production: process.env.SITE_URL,
  }[process.env.NODE_ENV]

  const staticPages = fs
    .readdirSync('pages')
    .filter((staticPage) => {
      return !['_app.tsx', '_app.css', '_document.tsx', '_error.tsx', 'sitemap.xml.ts', 'welcome.tsx'].includes(
        staticPage
      )
    })
    .map((staticPagePath) => {
      return `${baseUrl}/${staticPagePath.substring(0, staticPagePath.length - 4)}`
    })

  const sitemap = `<?xml version="1.0" encoding="UTF-8"?>
    <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
      ${staticPages
        .map((url) => {
          return `
								<url>
									<loc>${url}</loc>
									<lastmod>${new Date().toISOString()}</lastmod>
									<changefreq>monthly</changefreq>
									<priority>1.0</priority>
								</url>
							`
        })
        .join('')}
    </urlset>
  `

  res.setHeader('Content-Type', 'text/xml')
  res.write(sitemap)
  res.end()

  return {
    props: {},
  }
}

const Sitemap = () => {}

export default Sitemap
