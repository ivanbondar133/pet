import React, { FC, useState } from 'react'
import { Box, Delta, Flex, Grid, GridItem, H1 } from 'components/helpers'
import Search from 'components/Forms/Search'
import Tabs, { TabItem } from 'components/helpers/Tabs'
import { createGetServerSideProps } from 'shared/server'
import { ParsedUrlQuery } from 'querystring'
import Filter from '../../components/pages/catalog/Filter'
import AppTable, { Column } from '../../components/Table'
import Dropdown from '../../components/Table/Components/Dropdown'
import { SortOptionsType } from '../../types'
import PaginatedItems from '../../components/Pagination'

export const getServerSideProps = createGetServerSideProps(async (context) => {
  return {
    props: { query: context.query },
  }
})

type Props = {
  query?: ParsedUrlQuery
}
const sortOptions: SortOptionsType = {
  sort_col2: [
    { label: 'По возрастанию', value: 'asc' },
    { label: 'По убыванию', value: 'desc' },
  ],
  time: [
    { label: 'Доходность за день', value: 'day' },
    { label: 'Доходность за год', value: 'year' },
  ],
}

const QUERY_PARAM = 'share_type'

const Training: FC<Props> = ({ query }) => {
  const tabs: TabItem[] = [
    {
      label: 'Акции',
      value: 'shares',
    },
    {
      label: 'Фонды',
      value: 'fonds',
    },
    {
      label: 'Облигации',
      value: 'bonds',
    },
    {
      label: 'Валюта',
      value: 'currency',
    },
  ]

  const [currentTab, setCurrentTab] = useState(query?.[QUERY_PARAM] as string)

  let title: string

  switch (currentTab) {
    case 'shares':
      title = 'Каталог акций'
      break
    case 'fonds':
      title = 'Каталог фондов'
      break
    case 'bonds':
      title = 'Каталог облигаций'
      break
    case 'currency':
    default:
      title = 'Каталог валют'
  }
  const data = [
    {
      name: 'Юнипро',
      delta: 0.01,
      price: 'World',
    },
    {
      name: 'Юнипро',
      delta: -0.01,
      price: '2,81₽',
    },
    {
      name: 'Юнипро',
      delta: 0.01,
      price: '2,81₽',
    },
  ]

  const columns: Column[] = [
    {
      Header: 'Название',
      accessor: 'name',
    },
    {
      Header: () => {
        return (
          <Flex alignItems="center">
            Изменение за день <Dropdown options={sortOptions} />
          </Flex>
        )
      },
      accessor: 'delta',
      sortable: true,
      Cell: ({ value }) => {
        return <Delta value={value} />
      },
    },
    {
      Header: () => {
        return (
          <Flex alignItems="center">
            Цена <Dropdown options={sortOptions} />
          </Flex>
        )
      },
      accessor: 'price',
      sortable: true,
    },
  ]

  return (
    <Box pt={8}>
      <Grid>
        <GridItem mb={7} span={[4, 4, 12, 12]}>
          <Search placeholder="Название или тикер" />
        </GridItem>
        <GridItem span={[4, 4, 3, 3]}>
          <Tabs
            current={currentTab}
            list={tabs}
            onChange={setCurrentTab}
            queryProp={QUERY_PARAM}
            shouldUpdateQuery
            vertical
          />
          <Filter />
        </GridItem>
        <GridItem span={[4, 4, 9, 9]}>
          <H1>{title}</H1>
          <AppTable columns={columns} data={data} tableStyle={{ width: '100%' }} />
          {/*<PaginatedItems itemsPerPage={4} />*/}
        </GridItem>
      </Grid>
    </Box>
  )
}

export default Training
