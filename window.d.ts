declare global {
  interface Window {
    [key as string]: any
  }
}

window.gtag = window.gtag || {};
