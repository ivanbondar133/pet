import { Color } from '../theme'

export type TImage = { src: string; width: number; height: number }
export type AnyObject = Record<string, any>

export type TagType = {
  variant: Color
  label: string
}

export type SortOptionsType = {
  [key: string]: Array<{ label: string; value: string }>
}
