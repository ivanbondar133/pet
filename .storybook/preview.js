import { ThemeProvider } from "styled-components"
import theme from "../theme";
import { addDecorator } from "@storybook/react"
import { withKnobs } from "@storybook/addon-knobs"
import GlobalStyle from "../theme/global-styles";

addDecorator(withKnobs)
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}

export const decorators = [
  (Story) => (
      <ThemeProvider theme={theme}>
        <GlobalStyle />
        <Story />
      </ThemeProvider>
  ),
]