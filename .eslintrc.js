module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true,
    jest: true,
  },
  extends: [
    "next",
    "eslint:recommended",
    "plugin:@typescript-eslint/eslint-recommended",
    "plugin:@typescript-eslint/recommended",
    "prettier",
    "plugin:react/recommended",
    "plugin:prettier/recommended",
  ],
  parser: "@typescript-eslint/parser",
  // parserOptions: {
  //   project: ["./tsconfig.json"],
  //   tsconfigRootDir: __dirname,
  //   sourceType: "module",
  //   ecmaFeatures: {
  //     jsx: true,
  //   },
  // },
  plugins: ["react", "react-hooks", "@typescript-eslint", "prettier", "cypress"],
  settings: {
    react: {
      version: "detect",
    },
  },
  ignorePatterns: ["src/typings/types/generated.ts", "*.test.js"],
  rules: {
    "no-case-declarations": "off",
    "no-console": "warn",
    "no-alert": "error",
    "no-debugger": "error",
    "max-len": ["off", { code: 150 }],
    "prefer-destructuring": ["error", { object: true, array: false }],
    "react/prop-types": "off",
    "react/display-name": "off",
    "react/self-closing-comp": "warn",
    "react-hooks/rules-of-hooks": "error",
    "react-hooks/exhaustive-deps": "warn",
    "react/jsx-pascal-case": ["error", { allowAllCaps: true, allowNamespace: true, ignore: ["_*"] }],
    "react/jsx-boolean-value": ["error"],
    "react/jsx-curly-brace-presence": ["error", "never"],
    "react/jsx-sort-props": "error",
    "@typescript-eslint/ban-ts-comment": "warn",
    "@typescript-eslint/interface-name-prefix": "off",
    "react/no-unescaped-entities": "off",
    "@typescript-eslint/naming-convention": [
      "error",
      {
        selector: "default",
        format: ["camelCase", "PascalCase", "UPPER_CASE"],
        leadingUnderscore: "allow",
      },
      { selector: "property", format: ["snake_case", "camelCase", "PascalCase"] },
      {
        selector: "enumMember",
        format: ["UPPER_CASE"],
      },
    ],
    "@typescript-eslint/no-use-before-define": "off",
    "@typescript-eslint/no-explicit-any": "off",
    "@typescript-eslint/no-non-null-assertion": "off",
    "@typescript-eslint/no-unused-vars": ["error", { varsIgnorePattern: "^_", argsIgnorePattern: "^_" }],
    "@typescript-eslint/camelcase": "off",
    "@typescript-eslint/member-delimiter-style": [
      "off",
      {
        multiline: {
          delimiter: "none",
          requireLast: true,
        },
        singleline: {
          delimiter: "semi",
          requireLast: false,
        },
      },
    ],
    "@typescript-eslint/explicit-function-return-type": "off",
    "prettier/prettier": "error",
    "react/react-in-jsx-scope": "off",
    "@typescript-eslint/explicit-module-boundary-types": 2,
    "cypress/no-assigning-return-values": "error",
    "cypress/no-force": "warn",
    "no-restricted-imports": [
      "off",
      {
        patterns: [
          {
            group: [".*"],
            message: "Use absolute paths for imports",
          },
        ],
      },
    ],
    'linebreak-style': 'off',
  },
}
