import { useInView } from 'react-intersection-observer'
import { AnimationControls, useAnimation } from 'framer-motion'

export default function useScroll(): [(node?: Element | null | undefined) => void, AnimationControls] {
  const animationControls = useAnimation()
  const [element, inView] = useInView({
    threshold: 0.1,
  })

  inView && animationControls.start('show') //: animationControls.start('hidden')

  return [element, animationControls]
}
