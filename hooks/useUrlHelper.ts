import { useRouter } from 'next/router'
import { AnyObject } from '../types'

type ReturnType = {
  updateQuery: (newParam: AnyObject | string) => void
  query: AnyObject
}

export const useUrlHelper = (): ReturnType => {
  const router = useRouter()

  const updateQuery = (param: AnyObject | string) => {
    const params = { ...router.query }
    if (typeof param === 'string') {
      delete params[param]
      router.push(
        {
          pathname: router.pathname,
          query: {
            ...params,
          },
        },
        null,
        {
          scroll: false,
        }
      )
    } else {
      router.push(
        {
          pathname: router.pathname,
          query: {
            ...params,
            ...param,
          },
        },
        null,
        {
          scroll: false,
        }
      )
    }
  }
  return { updateQuery, query: router.query }
}
