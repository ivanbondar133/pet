import { TypographyProps } from 'styled-system'

export interface Fonts {
  montserrat: string
  rubik: string
  roboto: string
}

export const fonts: Fonts = {
  montserrat: 'Montserrat',
  rubik: 'Rubik',
  roboto: 'Roboto',
}

type TypographySchemes = 'h1' | 'h2' | 'h3' | 'h4' | 'pTiny' | 'pSmall' | 'p' | 'pBig' | 'roboto'

type FontConfigurations = { [key in TypographySchemes]: TypographyProps }

export const FontConfigurations: FontConfigurations = {
  h1: {
    fontFamily: 'rubik',
    fontSize: [7, 8, 10],
    lineHeight: 0,
    fontWeight: 700,
  },
  h2: {
    fontFamily: 'rubik',
    fontSize: [6, 7, 9],
    lineHeight: 1,
    fontWeight: 700,
  },
  h3: {
    fontFamily: 'montserrat',
    fontSize: 5,
    lineHeight: 1,
    fontWeight: 600,
  },
  h4: {
    fontFamily: 'montserrat',
    fontSize: 4,
    lineHeight: 1,
    fontWeight: 600,
  },
  pTiny: {
    fontFamily: 'montserrat',
    fontSize: 0,
    lineHeight: 3,
  },
  pSmall: {
    fontFamily: 'montserrat',
    fontSize: 1,
    lineHeight: 3,
  },
  p: {
    fontFamily: 'montserrat',
    fontSize: 2,
    lineHeight: 3,
    fontWeight: 400,
  },
  pBig: {
    fontFamily: 'montserrat',
    fontSize: 3,
    lineHeight: 3,
    fontWeight: 500,
  },
  roboto: {
    fontFamily: 'roboto',
    fontWeight: 400,
    fontSize: 3,
    lineHeight: 3,
  },
}
