import { createGlobalStyle } from 'styled-components'
import { ThemeInterface } from './theme'

export type ThemeProps = {
  theme: ThemeInterface
}

const GlobalStyle = createGlobalStyle<ThemeProps>`
  * {
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }
  
  .pointer {
    cursor: pointer;
  } 
  
  ul, li {
    list-style: none;
  }
  
  html {
    scroll-behavior: smooth;
  }

  body {
    color: ${({ theme }) => theme.colors.grayDark};
    font-family: ${({ theme }) => theme.fonts.montserrat};
    scroll-behavior: smooth;
  }
  
  table {
    border-collapse: collapse;
  }

  .check {
    position: absolute;
    top: 0;
    left: 0;
    overflow: hidden;
    visibility: hidden;
  }
  .parallax-global {
    z-index: -1;
    position: absolute;
  }
  .p-chart {
    left: -250px;
    top: 0;
  }
  .p-hiw1 {
    right: -400px;
    top: 0px;
  }
  .p-hiw2 {
    left: -300px;
    bottom: -450px;
  }
  .p-hiw3 {
    left: 0;
    bottom: -200px;
  }
  .p-hte1 {
    right: -100px;
    top: -100px;
  }
  .p-hte2 {
    left: -100px;
    top: 400px;
  }
  .p-hte3 {
    left: 50px;
    top: 400px;
  }
  .p-prices {
    left: -180px;
    bottom: -200px;
  }
  #outline {
    stroke-dasharray: 242*0.01, 242;
    stroke-dashoffset: 0;
    animation: anim 1.6s linear infinite;
  }
  @keyframes anim {
    12.5% {
      stroke-dasharray: 242*0.14, 242;
      stroke-dashoffset: -242*0.11;
    }
    43.75% {
      stroke-dasharray: 242*0.35, 242;
      stroke-dashoffset: -242*0.35;
    }
    100% {
      stroke-dasharray: 242*0.01, 242;
      stroke-dashoffset: -242*0.99;
    }
  }
  .preload {
    position: fixed;
    top: 0;
    left: 0;
    background: #fff;
    z-index: 10001;
    width: 100%;
    height: 100vh;
  }
`

export default GlobalStyle
