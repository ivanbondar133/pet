import theme from './theme'

export * from './palette'
export * from './typography'
export * from './theme'

export default theme
