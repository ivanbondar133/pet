import { style } from 'styled-system'

export const layer = style({
  prop: 'layer',
  cssProperty: 'zIndex',
  key: 'layers',
})
