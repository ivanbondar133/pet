import { colors, Palette } from './palette'
import { fonts, Fonts } from './typography'

export interface ThemeInterface {
  colors: Palette
  breakpoints: string[]
  mediaQueries: { [key: string]: string }
  space: string[]
  layers: string[]
  maxWidths: Array<string | number>
  fonts: Fonts
  fontSizes: string[]
  lineHeights: number[]
  letterSpacings: string[]
}

export const GUTTER = 15
export const BORDER_RADIUS = 10

export const breakpointsAsInts = [768, 1024, 1200]

export const breakpoints = breakpointsAsInts.map((bp) => `${bp}px`)

export const mediaQueries = {
  small: `@media screen and (max-width: ${breakpointsAsInts[0] - 1}px)`,
  medium: `@media screen and (min-width: ${breakpoints[0]})`,
  large: `@media screen and (min-width: ${breakpoints[1]})`,
  xLarge: `@media screen and (min-width: ${breakpoints[2]})`,
}

export const space = [
  '0rem', // 0 0px
  '0.3125rem', // 1 5px
  '0.6875rem', // 2 11px
  '0.8750rem', // 3 14px
  '1rem', // 4 16px
  '1.25rem', // 5 20px
  '1.5rem', // 6 24px
  '2.25rem', // 7 36px
  '3.375rem', // 8 54px
  '5.0625rem', // 9 81px
  '12.5rem', // 10 200px
]
export const fontSizes = [
  '0.8125rem', // 0 13px
  '0.8750rem', // 1 14px
  '1rem', // 2 16px
  '1.125rem', // 3 18px
  '1.25rem', // 4 20px
  '1.5rem', // 5 24px
  '1.75rem', // 6 28px
  '1.875rem', // 7 30px
  '2rem', // 8 32px
  '2.375rem', // 9 38px
  '3.125rem', // 10 50px
]

/* Standardizing line-heights across fonts */
export const lineHeights = [1, 1.2, 1.25, 1.5]

export const letterSpacings = ['0', '0.025em', '0.05em', '0.075em', '0.1em', '0.15em']

/* Max-widths based on breakpoints */
export const maxWidths = ['none', 748, 974, 1200]

export const layers = ['default', 'middle', 'top', 'notification', 'backdrop', 'drawer', 'menu', 'modal']

const theme: ThemeInterface = {
  colors,
  breakpoints,
  mediaQueries,
  space,
  layers,
  maxWidths,
  fonts,
  fontSizes,
  lineHeights,
  letterSpacings,
}

export default theme
