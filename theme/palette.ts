export type Color =
  | 'primary'
  | 'primaryLight'
  | 'primaryDark'
  | 'pink'
  | 'accent'
  | 'accentLight'
  | 'accentDark'
  | 'gray'
  | 'grayLight'
  | 'grayLighter'
  | 'grayDark'
  | 'success'
  | 'error'
  | 'white'
  | 'warning'
  | 'black'
  | 'cardsBg'

export type Palette = { [key in Color]: string }

/* Naming syntax is color[Value] */
export const colors: Palette = {
  primary: '#FF1167',
  primaryLight: '#FFAFCC',
  primaryDark: '#DE0D58',
  pink: '#FFEFF3',
  accent: '#7B61FF',
  accentLight: '#B4A6FF',
  accentDark: '#5D3EFF',
  gray: '#8E8E8E',
  grayLight: '#DBDBDB',
  grayLighter: '#F2F2F2',
  grayDark: '#323232',
  success: '#04C700',
  error: '#DC0000',
  warning: '#FFB802',
  white: '#FFFFFF',
  black: '#000000',
  cardsBg: '#F9F9F9', // Should to remove this.
}
