const { withSentryConfig } = require('@sentry/nextjs')

const config = {
  reactStrictMode: false,
  async redirects() {
    let routes = []
    if ('production' === process.env.NODE_ENV) {
      routes.push({
        source: '/welcome',
        destination: '/',
        permanent: false,
      })
    }

    return routes
  },
  sentry: {
    disableServerWebpackPlugin: true,
    disableClientWebpackPlugin: true,
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
}

const SentryWebpackPluginOptions = {
  // Additional config options for the Sentry Webpack plugin. Keep in mind that
  // the following options are set automatically, and overriding them is not
  // recommended:
  //   release, url, org, project, authToken, configFile, stripPrefix,
  //   urlPrefix, include, ignore

  silent: true, // Suppresses all logs
  // For all available options, see:
  // https://github.com/getsentry/sentry-webpack-plugin#options.
}

module.exports = withSentryConfig(config, SentryWebpackPluginOptions)
